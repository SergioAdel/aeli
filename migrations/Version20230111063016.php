<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230111063016 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ai_course (id INT AUTO_INCREMENT NOT NULL, teaching_unit_id INT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, credit VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) NOT NULL, INDEX IDX_10746D9D2F82B390 (teaching_unit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_course_contents (id INT AUTO_INCREMENT NOT NULL, course_id INT NOT NULL, content_type VARCHAR(255) NOT NULL, content LONGTEXT DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, course_type VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_F1FFB2B7591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_degrees (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_exam_sessions (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_exams (id INT AUTO_INCREMENT NOT NULL, course_id INT NOT NULL, session_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, duration INT NOT NULL, question LONGTEXT DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, INDEX IDX_3586286D591CC992 (course_id), INDEX IDX_3586286D613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_faculties (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, acronym VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_levels (id INT AUTO_INCREMENT NOT NULL, degree_id INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_99C0663DB35C5756 (degree_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_messages (id INT AUTO_INCREMENT NOT NULL, sender_id INT NOT NULL, receiver_id INT NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', file VARCHAR(255) DEFAULT NULL, INDEX IDX_C4E498F6F624B39D (sender_id), INDEX IDX_C4E498F6CD53EDB6 (receiver_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_pages (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, view VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, is_deleted TINYINT(1) NOT NULL, link_text VARCHAR(255) NOT NULL, menu_order INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_page_ai_page (ai_page_source INT NOT NULL, ai_page_target INT NOT NULL, INDEX IDX_B189D5A05C3B3AD2 (ai_page_source), INDEX IDX_B189D5A045DE6A5D (ai_page_target), PRIMARY KEY(ai_page_source, ai_page_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_payments (id INT AUTO_INCREMENT NOT NULL, student_id INT NOT NULL, reason VARCHAR(255) NOT NULL, state VARCHAR(255) DEFAULT NULL, decision VARCHAR(255) DEFAULT NULL, amount VARCHAR(255) DEFAULT NULL, observation VARCHAR(255) DEFAULT NULL, validated_at DATETIME DEFAULT NULL, done_at DATE NOT NULL, reference_mobile_money VARCHAR(255) DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, INDEX IDX_7A341D52CB944F1A (student_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_results (id INT AUTO_INCREMENT NOT NULL, student_id INT NOT NULL, session_exam_id INT NOT NULL, course_id INT NOT NULL, response LONGTEXT DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, note DOUBLE PRECISION DEFAULT NULL, begin_at DATETIME DEFAULT NULL, INDEX IDX_A3A6EAC7CB944F1A (student_id), INDEX IDX_A3A6EAC74555DCCB (session_exam_id), INDEX IDX_A3A6EAC7591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_semesters (id INT AUTO_INCREMENT NOT NULL, level_id INT NOT NULL, number INT NOT NULL, year INT NOT NULL, INDEX IDX_88EFEE9E5FB14BA7 (level_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_student_informations (id INT AUTO_INCREMENT NOT NULL, level_id INT NOT NULL, faculty_id INT NOT NULL, semester_id INT DEFAULT NULL, vague_id INT DEFAULT NULL, matricule_number VARCHAR(255) DEFAULT NULL, employer_name VARCHAR(255) DEFAULT NULL, job VARCHAR(255) DEFAULT NULL, fm_names VARCHAR(255) DEFAULT NULL, fm_jobs VARCHAR(255) DEFAULT NULL, fm_email VARCHAR(255) DEFAULT NULL, fm_phone_number VARCHAR(255) DEFAULT NULL, fm_adress VARCHAR(255) DEFAULT NULL, t_names VARCHAR(255) DEFAULT NULL, t_jobs VARCHAR(255) DEFAULT NULL, t_email VARCHAR(255) DEFAULT NULL, t_phone_number VARCHAR(255) DEFAULT NULL, t_adress VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, g_names VARCHAR(255) DEFAULT NULL, g_email VARCHAR(255) DEFAULT NULL, g_phone_number VARCHAR(255) DEFAULT NULL, formation_type VARCHAR(255) NOT NULL, cin_photo VARCHAR(255) DEFAULT NULL, curriculum_vitae VARCHAR(255) DEFAULT NULL, note_or_copy_of_the_certified_diploma VARCHAR(255) DEFAULT NULL, residence_certificate VARCHAR(255) DEFAULT NULL, birth_certificate VARCHAR(255) DEFAULT NULL, motivation_letter VARCHAR(255) DEFAULT NULL, sexe VARCHAR(255) DEFAULT NULL, native_country VARCHAR(255) DEFAULT NULL, registration_fee INT DEFAULT NULL, monthly_fees INT DEFAULT NULL, examination_fees INT DEFAULT NULL, defense_fee INT DEFAULT NULL, certificate_fee INT DEFAULT NULL, INDEX IDX_8CBB67C5FB14BA7 (level_id), INDEX IDX_8CBB67C680CAB68 (faculty_id), INDEX IDX_8CBB67C4A798B6F (semester_id), INDEX IDX_8CBB67C93E74B61 (vague_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_taughs (id INT AUTO_INCREMENT NOT NULL, faculty_id INT NOT NULL, semester_id INT NOT NULL, teacher_information_id INT NOT NULL, course_id INT DEFAULT NULL, month INT NOT NULL, INDEX IDX_F698FE8F680CAB68 (faculty_id), INDEX IDX_F698FE8F4A798B6F (semester_id), INDEX IDX_F698FE8F7950C5AA (teacher_information_id), INDEX IDX_F698FE8F591CC992 (course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_teacher_informations (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, salary VARCHAR(255) DEFAULT NULL, INDEX IDX_7CAF3F9A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_teaching_units (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_timetables (id INT AUTO_INCREMENT NOT NULL, faculty_id INT DEFAULT NULL, vague_id INT DEFAULT NULL, semester_id INT DEFAULT NULL, level_id INT DEFAULT NULL, start_at DATETIME NOT NULL, end_at DATETIME NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, target VARCHAR(255) DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, INDEX IDX_2A2B5D95680CAB68 (faculty_id), INDEX IDX_2A2B5D9593E74B61 (vague_id), INDEX IDX_2A2B5D954A798B6F (semester_id), INDEX IDX_2A2B5D955FB14BA7 (level_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_users (id INT AUTO_INCREMENT NOT NULL, student_information_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, password VARCHAR(255) NOT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, birth_date DATETIME DEFAULT NULL, birth_place VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, avatar VARCHAR(255) DEFAULT NULL, confirm_at DATETIME DEFAULT NULL, is_deleted TINYINT(1) NOT NULL, is_verified TINYINT(1) NOT NULL, status TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, UNIQUE INDEX UNIQ_48349EACE7927C74 (email), INDEX IDX_48349EAC391213BA (student_information_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_user_ai_role (ai_user_id INT NOT NULL, ai_role_id INT NOT NULL, INDEX IDX_50871C759B6BDD46 (ai_user_id), INDEX IDX_50871C75EA062C7F (ai_role_id), PRIMARY KEY(ai_user_id, ai_role_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ai_vagues (id INT AUTO_INCREMENT NOT NULL, v_number INT NOT NULL, v_description LONGTEXT DEFAULT NULL, start_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ai_course ADD CONSTRAINT FK_10746D9D2F82B390 FOREIGN KEY (teaching_unit_id) REFERENCES ai_teaching_units (id)');
        $this->addSql('ALTER TABLE ai_course_contents ADD CONSTRAINT FK_F1FFB2B7591CC992 FOREIGN KEY (course_id) REFERENCES ai_course (id)');
        $this->addSql('ALTER TABLE ai_exams ADD CONSTRAINT FK_3586286D591CC992 FOREIGN KEY (course_id) REFERENCES ai_course (id)');
        $this->addSql('ALTER TABLE ai_exams ADD CONSTRAINT FK_3586286D613FECDF FOREIGN KEY (session_id) REFERENCES ai_exam_sessions (id)');
        $this->addSql('ALTER TABLE ai_levels ADD CONSTRAINT FK_99C0663DB35C5756 FOREIGN KEY (degree_id) REFERENCES ai_degrees (id)');
        $this->addSql('ALTER TABLE ai_messages ADD CONSTRAINT FK_C4E498F6F624B39D FOREIGN KEY (sender_id) REFERENCES ai_users (id)');
        $this->addSql('ALTER TABLE ai_messages ADD CONSTRAINT FK_C4E498F6CD53EDB6 FOREIGN KEY (receiver_id) REFERENCES ai_users (id)');
        $this->addSql('ALTER TABLE ai_page_ai_page ADD CONSTRAINT FK_B189D5A05C3B3AD2 FOREIGN KEY (ai_page_source) REFERENCES ai_pages (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ai_page_ai_page ADD CONSTRAINT FK_B189D5A045DE6A5D FOREIGN KEY (ai_page_target) REFERENCES ai_pages (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ai_payments ADD CONSTRAINT FK_7A341D52CB944F1A FOREIGN KEY (student_id) REFERENCES ai_student_informations (id)');
        $this->addSql('ALTER TABLE ai_results ADD CONSTRAINT FK_A3A6EAC7CB944F1A FOREIGN KEY (student_id) REFERENCES ai_student_informations (id)');
        $this->addSql('ALTER TABLE ai_results ADD CONSTRAINT FK_A3A6EAC74555DCCB FOREIGN KEY (session_exam_id) REFERENCES ai_exam_sessions (id)');
        $this->addSql('ALTER TABLE ai_results ADD CONSTRAINT FK_A3A6EAC7591CC992 FOREIGN KEY (course_id) REFERENCES ai_course (id)');
        $this->addSql('ALTER TABLE ai_semesters ADD CONSTRAINT FK_88EFEE9E5FB14BA7 FOREIGN KEY (level_id) REFERENCES ai_levels (id)');
        $this->addSql('ALTER TABLE ai_student_informations ADD CONSTRAINT FK_8CBB67C5FB14BA7 FOREIGN KEY (level_id) REFERENCES ai_levels (id)');
        $this->addSql('ALTER TABLE ai_student_informations ADD CONSTRAINT FK_8CBB67C680CAB68 FOREIGN KEY (faculty_id) REFERENCES ai_faculties (id)');
        $this->addSql('ALTER TABLE ai_student_informations ADD CONSTRAINT FK_8CBB67C4A798B6F FOREIGN KEY (semester_id) REFERENCES ai_semesters (id)');
        $this->addSql('ALTER TABLE ai_student_informations ADD CONSTRAINT FK_8CBB67C93E74B61 FOREIGN KEY (vague_id) REFERENCES ai_vagues (id)');
        $this->addSql('ALTER TABLE ai_taughs ADD CONSTRAINT FK_F698FE8F680CAB68 FOREIGN KEY (faculty_id) REFERENCES ai_faculties (id)');
        $this->addSql('ALTER TABLE ai_taughs ADD CONSTRAINT FK_F698FE8F4A798B6F FOREIGN KEY (semester_id) REFERENCES ai_semesters (id)');
        $this->addSql('ALTER TABLE ai_taughs ADD CONSTRAINT FK_F698FE8F7950C5AA FOREIGN KEY (teacher_information_id) REFERENCES ai_teacher_informations (id)');
        $this->addSql('ALTER TABLE ai_taughs ADD CONSTRAINT FK_F698FE8F591CC992 FOREIGN KEY (course_id) REFERENCES ai_course (id)');
        $this->addSql('ALTER TABLE ai_teacher_informations ADD CONSTRAINT FK_7CAF3F9A76ED395 FOREIGN KEY (user_id) REFERENCES ai_users (id)');
        $this->addSql('ALTER TABLE ai_timetables ADD CONSTRAINT FK_2A2B5D95680CAB68 FOREIGN KEY (faculty_id) REFERENCES ai_faculties (id)');
        $this->addSql('ALTER TABLE ai_timetables ADD CONSTRAINT FK_2A2B5D9593E74B61 FOREIGN KEY (vague_id) REFERENCES ai_vagues (id)');
        $this->addSql('ALTER TABLE ai_timetables ADD CONSTRAINT FK_2A2B5D954A798B6F FOREIGN KEY (semester_id) REFERENCES ai_semesters (id)');
        $this->addSql('ALTER TABLE ai_timetables ADD CONSTRAINT FK_2A2B5D955FB14BA7 FOREIGN KEY (level_id) REFERENCES ai_levels (id)');
        $this->addSql('ALTER TABLE ai_users ADD CONSTRAINT FK_48349EAC391213BA FOREIGN KEY (student_information_id) REFERENCES ai_student_informations (id)');
        $this->addSql('ALTER TABLE ai_user_ai_role ADD CONSTRAINT FK_50871C759B6BDD46 FOREIGN KEY (ai_user_id) REFERENCES ai_users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ai_user_ai_role ADD CONSTRAINT FK_50871C75EA062C7F FOREIGN KEY (ai_role_id) REFERENCES ai_roles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES ai_users (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ai_course_contents DROP FOREIGN KEY FK_F1FFB2B7591CC992');
        $this->addSql('ALTER TABLE ai_exams DROP FOREIGN KEY FK_3586286D591CC992');
        $this->addSql('ALTER TABLE ai_results DROP FOREIGN KEY FK_A3A6EAC7591CC992');
        $this->addSql('ALTER TABLE ai_taughs DROP FOREIGN KEY FK_F698FE8F591CC992');
        $this->addSql('ALTER TABLE ai_levels DROP FOREIGN KEY FK_99C0663DB35C5756');
        $this->addSql('ALTER TABLE ai_exams DROP FOREIGN KEY FK_3586286D613FECDF');
        $this->addSql('ALTER TABLE ai_results DROP FOREIGN KEY FK_A3A6EAC74555DCCB');
        $this->addSql('ALTER TABLE ai_student_informations DROP FOREIGN KEY FK_8CBB67C680CAB68');
        $this->addSql('ALTER TABLE ai_taughs DROP FOREIGN KEY FK_F698FE8F680CAB68');
        $this->addSql('ALTER TABLE ai_timetables DROP FOREIGN KEY FK_2A2B5D95680CAB68');
        $this->addSql('ALTER TABLE ai_semesters DROP FOREIGN KEY FK_88EFEE9E5FB14BA7');
        $this->addSql('ALTER TABLE ai_student_informations DROP FOREIGN KEY FK_8CBB67C5FB14BA7');
        $this->addSql('ALTER TABLE ai_timetables DROP FOREIGN KEY FK_2A2B5D955FB14BA7');
        $this->addSql('ALTER TABLE ai_page_ai_page DROP FOREIGN KEY FK_B189D5A05C3B3AD2');
        $this->addSql('ALTER TABLE ai_page_ai_page DROP FOREIGN KEY FK_B189D5A045DE6A5D');
        $this->addSql('ALTER TABLE ai_user_ai_role DROP FOREIGN KEY FK_50871C75EA062C7F');
        $this->addSql('ALTER TABLE ai_student_informations DROP FOREIGN KEY FK_8CBB67C4A798B6F');
        $this->addSql('ALTER TABLE ai_taughs DROP FOREIGN KEY FK_F698FE8F4A798B6F');
        $this->addSql('ALTER TABLE ai_timetables DROP FOREIGN KEY FK_2A2B5D954A798B6F');
        $this->addSql('ALTER TABLE ai_payments DROP FOREIGN KEY FK_7A341D52CB944F1A');
        $this->addSql('ALTER TABLE ai_results DROP FOREIGN KEY FK_A3A6EAC7CB944F1A');
        $this->addSql('ALTER TABLE ai_users DROP FOREIGN KEY FK_48349EAC391213BA');
        $this->addSql('ALTER TABLE ai_taughs DROP FOREIGN KEY FK_F698FE8F7950C5AA');
        $this->addSql('ALTER TABLE ai_course DROP FOREIGN KEY FK_10746D9D2F82B390');
        $this->addSql('ALTER TABLE ai_messages DROP FOREIGN KEY FK_C4E498F6F624B39D');
        $this->addSql('ALTER TABLE ai_messages DROP FOREIGN KEY FK_C4E498F6CD53EDB6');
        $this->addSql('ALTER TABLE ai_teacher_informations DROP FOREIGN KEY FK_7CAF3F9A76ED395');
        $this->addSql('ALTER TABLE ai_user_ai_role DROP FOREIGN KEY FK_50871C759B6BDD46');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE ai_student_informations DROP FOREIGN KEY FK_8CBB67C93E74B61');
        $this->addSql('ALTER TABLE ai_timetables DROP FOREIGN KEY FK_2A2B5D9593E74B61');
        $this->addSql('DROP TABLE ai_course');
        $this->addSql('DROP TABLE ai_course_contents');
        $this->addSql('DROP TABLE ai_degrees');
        $this->addSql('DROP TABLE ai_exam_sessions');
        $this->addSql('DROP TABLE ai_exams');
        $this->addSql('DROP TABLE ai_faculties');
        $this->addSql('DROP TABLE ai_levels');
        $this->addSql('DROP TABLE ai_messages');
        $this->addSql('DROP TABLE ai_pages');
        $this->addSql('DROP TABLE ai_page_ai_page');
        $this->addSql('DROP TABLE ai_payments');
        $this->addSql('DROP TABLE ai_results');
        $this->addSql('DROP TABLE ai_roles');
        $this->addSql('DROP TABLE ai_semesters');
        $this->addSql('DROP TABLE ai_student_informations');
        $this->addSql('DROP TABLE ai_taughs');
        $this->addSql('DROP TABLE ai_teacher_informations');
        $this->addSql('DROP TABLE ai_teaching_units');
        $this->addSql('DROP TABLE ai_timetables');
        $this->addSql('DROP TABLE ai_users');
        $this->addSql('DROP TABLE ai_user_ai_role');
        $this->addSql('DROP TABLE ai_vagues');
        $this->addSql('DROP TABLE reset_password_request');
    }
}

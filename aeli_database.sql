-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 11 jan. 2023 à 09:38
-- Version du serveur :  8.0.21
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `aeli_database`
--

-- --------------------------------------------------------

--
-- Structure de la table `ai_course`
--

DROP TABLE IF EXISTS `ai_course`;
CREATE TABLE IF NOT EXISTS `ai_course` (
  `id` int NOT NULL AUTO_INCREMENT,
  `teaching_unit_id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_10746D9D2F82B390` (`teaching_unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_course_contents`
--

DROP TABLE IF EXISTS `ai_course_contents`;
CREATE TABLE IF NOT EXISTS `ai_course_contents` (
  `id` int NOT NULL AUTO_INCREMENT,
  `course_id` int NOT NULL,
  `content_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_F1FFB2B7591CC992` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_degrees`
--

DROP TABLE IF EXISTS `ai_degrees`;
CREATE TABLE IF NOT EXISTS `ai_degrees` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ai_degrees`
--

INSERT INTO `ai_degrees` (`id`, `name`, `description`) VALUES
(1, 'LICENCE', 'Diplome de licence'),
(2, 'MASTER', 'Diplome de master');

-- --------------------------------------------------------

--
-- Structure de la table `ai_exams`
--

DROP TABLE IF EXISTS `ai_exams`;
CREATE TABLE IF NOT EXISTS `ai_exams` (
  `id` int NOT NULL AUTO_INCREMENT,
  `course_id` int NOT NULL,
  `session_id` int DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int NOT NULL,
  `question` longtext COLLATE utf8mb4_unicode_ci,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3586286D591CC992` (`course_id`),
  KEY `IDX_3586286D613FECDF` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_exam_sessions`
--

DROP TABLE IF EXISTS `ai_exam_sessions`;
CREATE TABLE IF NOT EXISTS `ai_exam_sessions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_faculties`
--

DROP TABLE IF EXISTS `ai_faculties`;
CREATE TABLE IF NOT EXISTS `ai_faculties` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronym` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ai_faculties`
--

INSERT INTO `ai_faculties` (`id`, `name`, `acronym`, `description`) VALUES
(1, 'Business Entrepreneurship et Leadership', 'BEL', 'Business Entrepreneurship et Leadership'),
(2, 'Stratégie d’Entreprise et Administration Financière ', 'SEAF', 'Stratégie d’Entreprise et Administration Financière '),
(3, 'Management International et Transformation Digital ', 'MITD', 'Management International et Transformation Digital '),
(4, 'Ressources Humaines et Communication ', 'RHC', 'Ressources Humaines et Communication ');

-- --------------------------------------------------------

--
-- Structure de la table `ai_levels`
--

DROP TABLE IF EXISTS `ai_levels`;
CREATE TABLE IF NOT EXISTS `ai_levels` (
  `id` int NOT NULL AUTO_INCREMENT,
  `degree_id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_99C0663DB35C5756` (`degree_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ai_levels`
--

INSERT INTO `ai_levels` (`id`, `degree_id`, `name`, `description`) VALUES
(1, 1, 'Licence 1', 'Licence 1'),
(2, 1, 'Licence 2', 'Licence 2'),
(3, 1, 'Licence 3', 'Licence 3\r\n'),
(4, 2, 'Master 1', 'Master 1'),
(5, 2, 'Master 2', 'Master 2');

-- --------------------------------------------------------

--
-- Structure de la table `ai_messages`
--

DROP TABLE IF EXISTS `ai_messages`;
CREATE TABLE IF NOT EXISTS `ai_messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sender_id` int NOT NULL,
  `receiver_id` int NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C4E498F6F624B39D` (`sender_id`),
  KEY `IDX_C4E498F6CD53EDB6` (`receiver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_pages`
--

DROP TABLE IF EXISTS `ai_pages`;
CREATE TABLE IF NOT EXISTS `ai_pages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `view` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `link_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_order` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ai_pages`
--

INSERT INTO `ai_pages` (`id`, `title`, `slug`, `description`, `view`, `is_active`, `is_deleted`, `link_text`, `menu_order`) VALUES
(1, 'aeli &#8211; PROPULSER L’ECOSYSTEME ENTREPRENEURIAL', 'welcome', 'Page d\'accueil; Aeli Africa - PROPULSER L\'ECOSYSTEME ENTREPRENEURIAL', 'page/index.html', 1, 0, 'Acceuil', 1),
(2, 'A propos-Aeli Africa', 'a-propos', 'a propos d\'Aeli Africa', 'page/apropos.html', 1, 0, 'A propos', 2),
(3, 'Notre institution-Aeli Africa', 'notre-institution', 'Page :notre institution', 'page/institution.html', 1, 0, 'Notre institution', 3),
(4, 'Nos formations -Aeli Africa', 'nos-formations', 'Page: nos formations', 'page/formations.html', 1, 0, 'Nos formations', 4),
(5, 'Nous contacter-Aeli Afirca', 'nous-contacter', 'Page: contact', 'page/contact.html', 1, 0, 'Nous contacter', 5),
(6, 'Authentification', 'connexion', 'page de connexion', 'security/login-student.html', 1, 0, 'connexion', 6);

-- --------------------------------------------------------

--
-- Structure de la table `ai_page_ai_page`
--

DROP TABLE IF EXISTS `ai_page_ai_page`;
CREATE TABLE IF NOT EXISTS `ai_page_ai_page` (
  `ai_page_source` int NOT NULL,
  `ai_page_target` int NOT NULL,
  PRIMARY KEY (`ai_page_source`,`ai_page_target`),
  KEY `IDX_B189D5A05C3B3AD2` (`ai_page_source`),
  KEY `IDX_B189D5A045DE6A5D` (`ai_page_target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_payments`
--

DROP TABLE IF EXISTS `ai_payments`;
CREATE TABLE IF NOT EXISTS `ai_payments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validated_at` datetime DEFAULT NULL,
  `done_at` date NOT NULL,
  `reference_mobile_money` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7A341D52CB944F1A` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_results`
--

DROP TABLE IF EXISTS `ai_results`;
CREATE TABLE IF NOT EXISTS `ai_results` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `session_exam_id` int NOT NULL,
  `course_id` int NOT NULL,
  `response` longtext COLLATE utf8mb4_unicode_ci,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` double DEFAULT NULL,
  `begin_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A3A6EAC7CB944F1A` (`student_id`),
  KEY `IDX_A3A6EAC74555DCCB` (`session_exam_id`),
  KEY `IDX_A3A6EAC7591CC992` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_roles`
--

DROP TABLE IF EXISTS `ai_roles`;
CREATE TABLE IF NOT EXISTS `ai_roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ai_roles`
--

INSERT INTO `ai_roles` (`id`, `name`, `description`) VALUES
(1, 'ROLE_STUDENT', 'Étudiant'),
(2, 'ROLE_ADMIN', 'Administrateur'),
(3, 'ROLE_TEACHER', 'Proffesseur'),
(4, 'ROLE_SCHOOL_MANAGER', 'Géstionnaire d\'écolage');

-- --------------------------------------------------------

--
-- Structure de la table `ai_semesters`
--

DROP TABLE IF EXISTS `ai_semesters`;
CREATE TABLE IF NOT EXISTS `ai_semesters` (
  `id` int NOT NULL AUTO_INCREMENT,
  `level_id` int NOT NULL,
  `number` int NOT NULL,
  `year` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_88EFEE9E5FB14BA7` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_student_informations`
--

DROP TABLE IF EXISTS `ai_student_informations`;
CREATE TABLE IF NOT EXISTS `ai_student_informations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `level_id` int NOT NULL,
  `faculty_id` int NOT NULL,
  `semester_id` int DEFAULT NULL,
  `vague_id` int DEFAULT NULL,
  `matricule_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_names` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_jobs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fm_adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_names` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_jobs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `g_names` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formation_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cin_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_or_copy_of_the_certified_diploma` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `residence_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motivation_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_fee` int DEFAULT NULL,
  `monthly_fees` int DEFAULT NULL,
  `examination_fees` int DEFAULT NULL,
  `defense_fee` int DEFAULT NULL,
  `certificate_fee` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8CBB67C5FB14BA7` (`level_id`),
  KEY `IDX_8CBB67C680CAB68` (`faculty_id`),
  KEY `IDX_8CBB67C4A798B6F` (`semester_id`),
  KEY `IDX_8CBB67C93E74B61` (`vague_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_taughs`
--

DROP TABLE IF EXISTS `ai_taughs`;
CREATE TABLE IF NOT EXISTS `ai_taughs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `faculty_id` int NOT NULL,
  `semester_id` int NOT NULL,
  `teacher_information_id` int NOT NULL,
  `course_id` int DEFAULT NULL,
  `month` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F698FE8F680CAB68` (`faculty_id`),
  KEY `IDX_F698FE8F4A798B6F` (`semester_id`),
  KEY `IDX_F698FE8F7950C5AA` (`teacher_information_id`),
  KEY `IDX_F698FE8F591CC992` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_teacher_informations`
--

DROP TABLE IF EXISTS `ai_teacher_informations`;
CREATE TABLE IF NOT EXISTS `ai_teacher_informations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7CAF3F9A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_teaching_units`
--

DROP TABLE IF EXISTS `ai_teaching_units`;
CREATE TABLE IF NOT EXISTS `ai_teaching_units` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_timetables`
--

DROP TABLE IF EXISTS `ai_timetables`;
CREATE TABLE IF NOT EXISTS `ai_timetables` (
  `id` int NOT NULL AUTO_INCREMENT,
  `faculty_id` int DEFAULT NULL,
  `vague_id` int DEFAULT NULL,
  `semester_id` int DEFAULT NULL,
  `level_id` int DEFAULT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2A2B5D95680CAB68` (`faculty_id`),
  KEY `IDX_2A2B5D9593E74B61` (`vague_id`),
  KEY `IDX_2A2B5D954A798B6F` (`semester_id`),
  KEY `IDX_2A2B5D955FB14BA7` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ai_users`
--

DROP TABLE IF EXISTS `ai_users`;
CREATE TABLE IF NOT EXISTS `ai_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_information_id` int DEFAULT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirm_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_48349EACE7927C74` (`email`),
  KEY `IDX_48349EAC391213BA` (`student_information_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ai_users`
--

INSERT INTO `ai_users` (`id`, `student_information_id`, `email`, `password`, `first_name`, `last_name`, `birth_date`, `birth_place`, `address`, `phone_number`, `avatar`, `confirm_at`, `is_deleted`, `is_verified`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'adminaeli@gmail.com', 'adminaeli@gmail.com', 'admin', 'aeli', '2000-01-11 09:37:38', 'Ankazomanga', '34 bis', '0340247895', NULL, NULL, 0, 1, 1, '2023-01-11 09:37:38', '2023-01-12 09:37:38'),
(4, NULL, 'admin@gmail.com', '$2y$13$D7hWZ2Lh.RRw3cHSvTC58uLdnDXVHh7JPXsqhM8CeG.TN6WoNXbsG', 'aeli', 'admin', '2000-11-01 13:04:17', 'tana', '34bis', '0321456856', 'pgo405.png', NULL, 0, 0, 0, '2022-11-09 06:59:37', '2023-01-11 09:28:25');

-- --------------------------------------------------------

--
-- Structure de la table `ai_user_ai_role`
--

DROP TABLE IF EXISTS `ai_user_ai_role`;
CREATE TABLE IF NOT EXISTS `ai_user_ai_role` (
  `ai_user_id` int NOT NULL,
  `ai_role_id` int NOT NULL,
  PRIMARY KEY (`ai_user_id`,`ai_role_id`),
  KEY `IDX_50871C759B6BDD46` (`ai_user_id`),
  KEY `IDX_50871C75EA062C7F` (`ai_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ai_user_ai_role`
--

INSERT INTO `ai_user_ai_role` (`ai_user_id`, `ai_role_id`) VALUES
(1, 2),
(4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `ai_vagues`
--

DROP TABLE IF EXISTS `ai_vagues`;
CREATE TABLE IF NOT EXISTS `ai_vagues` (
  `id` int NOT NULL AUTO_INCREMENT,
  `v_number` int NOT NULL,
  `v_description` longtext COLLATE utf8mb4_unicode_ci,
  `start_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20230111063016', '2023-01-11 06:30:53', 196050);

-- --------------------------------------------------------

--
-- Structure de la table `reset_password_request`
--

DROP TABLE IF EXISTS `reset_password_request`;
CREATE TABLE IF NOT EXISTS `reset_password_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `selector` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashed_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `expires_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_7CE748AA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `ai_course`
--
ALTER TABLE `ai_course`
  ADD CONSTRAINT `FK_10746D9D2F82B390` FOREIGN KEY (`teaching_unit_id`) REFERENCES `ai_teaching_units` (`id`);

--
-- Contraintes pour la table `ai_course_contents`
--
ALTER TABLE `ai_course_contents`
  ADD CONSTRAINT `FK_F1FFB2B7591CC992` FOREIGN KEY (`course_id`) REFERENCES `ai_course` (`id`);

--
-- Contraintes pour la table `ai_exams`
--
ALTER TABLE `ai_exams`
  ADD CONSTRAINT `FK_3586286D591CC992` FOREIGN KEY (`course_id`) REFERENCES `ai_course` (`id`),
  ADD CONSTRAINT `FK_3586286D613FECDF` FOREIGN KEY (`session_id`) REFERENCES `ai_exam_sessions` (`id`);

--
-- Contraintes pour la table `ai_levels`
--
ALTER TABLE `ai_levels`
  ADD CONSTRAINT `FK_99C0663DB35C5756` FOREIGN KEY (`degree_id`) REFERENCES `ai_degrees` (`id`);

--
-- Contraintes pour la table `ai_messages`
--
ALTER TABLE `ai_messages`
  ADD CONSTRAINT `FK_C4E498F6CD53EDB6` FOREIGN KEY (`receiver_id`) REFERENCES `ai_users` (`id`),
  ADD CONSTRAINT `FK_C4E498F6F624B39D` FOREIGN KEY (`sender_id`) REFERENCES `ai_users` (`id`);

--
-- Contraintes pour la table `ai_page_ai_page`
--
ALTER TABLE `ai_page_ai_page`
  ADD CONSTRAINT `FK_B189D5A045DE6A5D` FOREIGN KEY (`ai_page_target`) REFERENCES `ai_pages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B189D5A05C3B3AD2` FOREIGN KEY (`ai_page_source`) REFERENCES `ai_pages` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `ai_payments`
--
ALTER TABLE `ai_payments`
  ADD CONSTRAINT `FK_7A341D52CB944F1A` FOREIGN KEY (`student_id`) REFERENCES `ai_student_informations` (`id`);

--
-- Contraintes pour la table `ai_results`
--
ALTER TABLE `ai_results`
  ADD CONSTRAINT `FK_A3A6EAC74555DCCB` FOREIGN KEY (`session_exam_id`) REFERENCES `ai_exam_sessions` (`id`),
  ADD CONSTRAINT `FK_A3A6EAC7591CC992` FOREIGN KEY (`course_id`) REFERENCES `ai_course` (`id`),
  ADD CONSTRAINT `FK_A3A6EAC7CB944F1A` FOREIGN KEY (`student_id`) REFERENCES `ai_student_informations` (`id`);

--
-- Contraintes pour la table `ai_semesters`
--
ALTER TABLE `ai_semesters`
  ADD CONSTRAINT `FK_88EFEE9E5FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `ai_levels` (`id`);

--
-- Contraintes pour la table `ai_student_informations`
--
ALTER TABLE `ai_student_informations`
  ADD CONSTRAINT `FK_8CBB67C4A798B6F` FOREIGN KEY (`semester_id`) REFERENCES `ai_semesters` (`id`),
  ADD CONSTRAINT `FK_8CBB67C5FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `ai_levels` (`id`),
  ADD CONSTRAINT `FK_8CBB67C680CAB68` FOREIGN KEY (`faculty_id`) REFERENCES `ai_faculties` (`id`),
  ADD CONSTRAINT `FK_8CBB67C93E74B61` FOREIGN KEY (`vague_id`) REFERENCES `ai_vagues` (`id`);

--
-- Contraintes pour la table `ai_taughs`
--
ALTER TABLE `ai_taughs`
  ADD CONSTRAINT `FK_F698FE8F4A798B6F` FOREIGN KEY (`semester_id`) REFERENCES `ai_semesters` (`id`),
  ADD CONSTRAINT `FK_F698FE8F591CC992` FOREIGN KEY (`course_id`) REFERENCES `ai_course` (`id`),
  ADD CONSTRAINT `FK_F698FE8F680CAB68` FOREIGN KEY (`faculty_id`) REFERENCES `ai_faculties` (`id`),
  ADD CONSTRAINT `FK_F698FE8F7950C5AA` FOREIGN KEY (`teacher_information_id`) REFERENCES `ai_teacher_informations` (`id`);

--
-- Contraintes pour la table `ai_teacher_informations`
--
ALTER TABLE `ai_teacher_informations`
  ADD CONSTRAINT `FK_7CAF3F9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `ai_users` (`id`);

--
-- Contraintes pour la table `ai_timetables`
--
ALTER TABLE `ai_timetables`
  ADD CONSTRAINT `FK_2A2B5D954A798B6F` FOREIGN KEY (`semester_id`) REFERENCES `ai_semesters` (`id`),
  ADD CONSTRAINT `FK_2A2B5D955FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `ai_levels` (`id`),
  ADD CONSTRAINT `FK_2A2B5D95680CAB68` FOREIGN KEY (`faculty_id`) REFERENCES `ai_faculties` (`id`),
  ADD CONSTRAINT `FK_2A2B5D9593E74B61` FOREIGN KEY (`vague_id`) REFERENCES `ai_vagues` (`id`);

--
-- Contraintes pour la table `ai_users`
--
ALTER TABLE `ai_users`
  ADD CONSTRAINT `FK_48349EAC391213BA` FOREIGN KEY (`student_information_id`) REFERENCES `ai_student_informations` (`id`);

--
-- Contraintes pour la table `ai_user_ai_role`
--
ALTER TABLE `ai_user_ai_role`
  ADD CONSTRAINT `FK_50871C759B6BDD46` FOREIGN KEY (`ai_user_id`) REFERENCES `ai_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_50871C75EA062C7F` FOREIGN KEY (`ai_role_id`) REFERENCES `ai_roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `reset_password_request`
--
ALTER TABLE `reset_password_request`
  ADD CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `ai_users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

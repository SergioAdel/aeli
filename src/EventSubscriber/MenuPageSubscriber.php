<?php

namespace App\EventSubscriber;

use Twig\Environment;
use App\Repository\AiPageRepository;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MenuPageSubscriber implements EventSubscriberInterface
{
    private $twig;
    private $aiPageRepository;
    public function __construct(Environment $twig, AiPageRepository $aiPageRepository)
    {
        $this->twig = $twig;
        $this->aiPageRepository = $aiPageRepository;
    }
    public function onControllerEvent(ControllerEvent  $event)
    {
        $this->twig->addGlobal('allPages', $this->aiPageRepository->getPages());
    }

    public static function getSubscribedEvents()
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
    }
}

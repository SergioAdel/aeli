<?php

namespace App\EventSubscriber;

use App\Controller\Student\HelpController;
use App\Controller\Student\CourseController;
use App\Controller\Student\ExamenController;
use App\Controller\Student\ProfilController;
use App\Controller\Student\MessageController;
use Symfony\Component\Security\Core\Security;
use App\Controller\Student\CalendarController;
use App\Controller\Student\ExerciceController;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ControllerStudentSubscriber implements EventSubscriberInterface
{
    private $security;
    private $router;
    private $flash;
    public function __construct(Security $security, RouterInterface $router, FlashBagInterface $flash)
    {
        $this->security = $security;
        $this->router = $router;
        $this->flash = $flash;
    }
    public function onControllerEvent(ControllerEvent $event): void
    {


        // test ecolage et les autres paiments
        $controller = $event->getController();
        if (is_array($controller)) {
            $controller = $controller[0];
        }
        if (
            $controller instanceof CalendarController ||
            $controller instanceof CourseController ||
            $controller instanceof ExamenController ||
            $controller instanceof ExerciceController ||
            $controller instanceof HelpController ||
            $controller instanceof MessageController ||
            $controller instanceof ProfilController
        ) {
            $studentInformation = $this->security->getUser()->getStudentInformation();
            $vague = $studentInformation->getVague();
            $registrationFee = $studentInformation->getRegistrationFee();
            $monthlyFees = $studentInformation->getMonthlyFees();
            if (!$registrationFee) {
                $redirection = new RedirectResponse($this->router->generate('app_payment_user_registration_fee'));
                $redirection->send();
            }
            if (!$monthlyFees) {
                $redirection = new RedirectResponse($this->router->generate('app_payment_monthly_fee'));
                $redirection->send();
            }
            $startedVague = $vague->getStartAt();
            if ($startedVague) {
                $now = new \DateTimeImmutable("now");
                $diff = $startedVague->diff($now, true);
                $monthToPay = ($diff->m) + 1;
                $feeToPay = $monthToPay - $monthlyFees;
                if ($feeToPay > 0) {
                    $this->flash->add('warning', 'Vous devez payer votre écolage du ' . $monthlyFees + 1 . 'ème mois');
                    $redirection = new RedirectResponse($this->router->generate('app_payment_monthly_fee'));
                    $redirection->send();
                }
            }
        }


        // test les examens
        if ($this->security->getUser() and $this->security->getUser()->getStudentInformation()) {

            $studentConnectedInformation  = $this->security->getUser()->getStudentInformation();
            $studentVague = $studentConnectedInformation->getVague();
            $startedDateEnter = $studentVague->getStartAt();
            $examMomentBegin = $startedDateEnter->add(new \DateInterval('P4M'));
            $examMomentEnd = $examMomentBegin->add(new \DateInterval('P06D'));

            // manao verification si le jour ou on est entre le date d'exam
            $today = new \DateTimeImmutable('now');

            // if ($examMomentEnd <= $today and $examMomentBegin >= $today) {
            //     dd("OK");
            // } else {
            //     dd('NO');
            // }

            // dd($startedDateEnter, $examMomentBegin, $examMomentEnd, $today);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
    }
}

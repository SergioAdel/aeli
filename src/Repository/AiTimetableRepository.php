<?php

namespace App\Repository;

use App\Entity\AiStudentInformation;
use App\Entity\AiTimetable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiTimetable>
 *
 * @method AiTimetable|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiTimetable|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiTimetable[]    findAll()
 * @method AiTimetable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiTimetableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiTimetable::class);
    }

    public function add(AiTimetable $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiTimetable $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return AiTimetable[] Returns an array of AiTimetable objects
     */
    public function filter($filter): array
    {
        $query = $this->createQueryBuilder('tt')
            ->innerJoin('tt.faculty', 'f', 'WITH', 'f = :faculty')
            ->setParameter('faculty', $filter['faculty'])
            ->andWhere('tt.target = :target')
            ->setParameter('target', $filter['target']);

        if ($filter['level']) {
            $query->innerJoin('tt.level', 'l', 'WITH', 'l = :level')
                ->setParameter('level', $filter['level']);
        }

        if ($filter['vague']) {
            $query->innerJoin('tt.vague', 'v', 'WITH', 'v = :vague')
                ->setParameter('vague', $filter['vague']);
        }
        if ($filter['semester']) {
            $query->innerJoin('tt.semester', 's', 'WITH', 's = :semester')
                ->setParameter('semester', $filter['semester']);
        }
        return
            $query
            ->orderBy('tt.id', 'ASC')
            ->getQuery()
            ->getResult();
    }



    /**
     * @return AiTimetable[] Returns an array of AiTimetable objects
     */
    public function getTimetables(AiStudentInformation $studentInformation): array
    {
        return $this->createQueryBuilder('tt')
            ->innerJoin('tt.faculty', 'f', 'WITH', 'f = :faculty')
            ->setParameter('faculty', $studentInformation->getFaculty())
            ->innerJoin('tt.level', 'l', 'WITH', 'l = :level')
            ->setParameter('level', $studentInformation->getLevel())
            ->innerJoin('tt.vague', 'v', 'WITH', 'v = :vague')
            ->setParameter('vague', $studentInformation->getVague())
            ->andWhere('tt.target = :target')
            ->setParameter('target', $studentInformation->getFormationType())
            ->orderBy('tt.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    //    public function findOneBySomeField($value): ?IeTimetable
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}

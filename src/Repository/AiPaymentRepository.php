<?php

namespace App\Repository;

use App\Entity\AiPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiPayment>
 *
 * @method AiPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiPayment[]    findAll()
 * @method AiPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiPayment::class);
    }

    public function add(AiPayment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiPayment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return AiPayment[] Returns an array of AiPayment objects
     */
    public function getRequestUnReadByType($type): array
    {
        return $this->createQueryBuilder('aip')
            ->andWhere("aip.type = :type AND aip.state = 'unread'")
            ->setParameter('type', $type)
            ->orderBy('aip.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiPayment[] Returns an array of AiPayment objects
     */
    public function getClassificationFilter(AiPayment $aiPayment): array
    {
        $query = $this->createQueryBuilder('aip');
        if ($aiPayment->getValidatedAt()) {
            $query->andWhere("aip.validatedAt = :validatedAt")
                ->setParameter('validatedAt', $aiPayment->getValidatedAt());
        }
        if ($aiPayment->getReason()) {
            $query->andWhere("aip.reason = :reason")
                ->setParameter('reason', $aiPayment->getReason());
        }
        if ($aiPayment->getType()) {
            $query->andWhere("aip.type = :type")
                ->setParameter('type', $aiPayment->getType());
        }
        if ($aiPayment->getState()) {
            $query->andWhere("aip.state = :state")
                ->setParameter('state', $aiPayment->getState());
        }
        if ($aiPayment->getDecision()) {
            $query->andWhere("aip.decision = :decision")
                ->setParameter('decision', $aiPayment->getDecision());
        }
        return $query
            ->orderBy('aip.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?IePayment
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}

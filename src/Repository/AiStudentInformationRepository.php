<?php

namespace App\Repository;

use App\Entity\AiFaculty;
use App\Entity\AiLevel;
use App\Entity\AiStudentInformation;
use App\Entity\AiUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiStudentInformation>
 *
 * @method AiStudentInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiStudentInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiStudentInformation[]    findAll()
 * @method AiStudentInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiStudentInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiStudentInformation::class);
    }

    public function add(AiStudentInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiStudentInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return AiStudentInformation[] Returns an array of AiStudentInformation objects
     */
    public function getMatriculeNumberByFaculty(AiFaculty $aiFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $aiFaculty)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiStudentInformation[] Returns an array of AiStudentInformation objects
     */
    public function getNumberStudentOfFaculty(AiFaculty $aiFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $aiFaculty)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return AiStudentInformation[] Returns an array of AiStudentInformation objects
     */
    public function getStudentByFormationTypeOfFaculty(AiFaculty $aiFaculty, string $formationType): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $aiFaculty)
            ->andWhere('si.formationType = :formationType')
            ->setParameter('formationType', $formationType)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiStudentInformation[] Returns an array of AiStudentInformation objects
     */
    public function getStudentMadagascarOfFaculty(AiFaculty $aiFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $aiFaculty)
            ->andWhere('si.nativeCountry = :nat')
            ->setParameter('nat', 'MG')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiStudentInformation[] Returns an array of AiStudentInformation objects
     */
    public function getStudentAbroadOfFaculty(AiFaculty $AiFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $AiFaculty)
            ->andWhere('si.nativeCountry != :nat')
            ->setParameter('nat', 'MG')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiStudentInformation[] Returns an array of AiStudentInformation objects
     */
    public function getStudentByLevelOfFaculty(AiFaculty $aiFaculty, string $level): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.level', 'l', 'WITH', 'l.name = :level')
            ->innerJoin('si.users', 'u')
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->setParameter('faculty', $aiFaculty)
            ->setParameter('level', $level)
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    public function getInformationByUser(AiUser $aiUser): ?AiStudentInformation
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.users', 'u', 'WITH', 'u.id = :user')
            ->setParameter('user', $aiUser->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return AiStudentInformation[] Returns an array of AiStudentInformation objects
     */
    public function getRegistratedStudentByFaculty(AiFaculty $aiFaculty): array
    {
        return $this->createQueryBuilder('si')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->innerJoin('si.users', 'u')
            ->setParameter('faculty', $aiFaculty)
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return AiStudentInformation[] Returns an array of AiStudentInformation objects
     */
    public function getFilterStudentInformation(AiStudentInformation $aiStudentInformation): array
    {
        // dd($ieStudentInformation);
        $queryB = $this->createQueryBuilder('si')
            ->innerJoin('si.users', 'u')
            ->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')
            ->setParameter('faculty', $aiStudentInformation->getFaculty());

        if ($aiStudentInformation->getType()) $queryB->andWhere('si.formationType = :type')->setParameter('type', $aiStudentInformation->getType());
        if ($aiStudentInformation->getLevel()) $queryB->innerJoin('si.level', 'l', 'WITH', 'l = :level')->setParameter('level', $aiStudentInformation->getLevel());
        if ($aiStudentInformation->getSemester()) $queryB->innerJoin('si.semester', 's', 'WITH', 's = :semester')->setParameter('semester', $aiStudentInformation->getSemester());
        if ($aiStudentInformation->getVague()) $queryB->innerJoin('si.vague', 'v', 'WITH', 'v = :vague')->setParameter('vague', $aiStudentInformation->getVague());

        return $queryB
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return AiStudentInformation[] Returns an array of AiStudentInformation objects
     */
    public function getPaymentStatusStudent(AiStudentInformation $aiStudentInformation): array
    {
        // dd($ieStudentInformation);
        $queryB = $this->createQueryBuilder('si')
            ->innerJoin('si.users', 'u');

        if ($aiStudentInformation->getType()) $queryB->andWhere('si.formationType = :type')->setParameter('type', $aiStudentInformation->getType());
        if ($aiStudentInformation->getLevel()) $queryB->innerJoin('si.level', 'l', 'WITH', 'l = :level')->setParameter('level', $aiStudentInformation->getLevel());
        if ($aiStudentInformation->getFaculty()) $queryB->innerJoin('si.faculty', 'f', 'WITH', 'f = :faculty')->setParameter('faculty', $aiStudentInformation->getFaculty());
        if ($aiStudentInformation->getSemester()) $queryB->innerJoin('si.semester', 's', 'WITH', 's = :semester')->setParameter('semester', $aiStudentInformation->getSemester());
        if ($aiStudentInformation->getVague()) $queryB->innerJoin('si.vague', 'v', 'WITH', 'v = :vague')->setParameter('vague', $aiStudentInformation->getVague());

        return $queryB
            ->andWhere('u.isDeleted = false AND u.confirmAt IS NOT NULL')
            ->andWhere('si.matriculeNumber IS NOT NULL')
            ->orderBy('si.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
}

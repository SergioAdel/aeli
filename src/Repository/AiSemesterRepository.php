<?php

namespace App\Repository;

use App\Entity\AiSemester;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiSemester>
 *
 * @method AiSemester|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiSemester|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiSemester[]    findAll()
 * @method AiSemester[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiSemesterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiSemester::class);
    }

    public function add(AiSemester $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiSemester $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    public function getSemesterByLevel($level)
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.level', 'u', 'WITH', 'u.id = :level')
            ->setParameter('level', $level->getId());
    }

    //    public function findOneBySomeField($value): ?IeSemester
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}

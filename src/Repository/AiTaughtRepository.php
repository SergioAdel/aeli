<?php

namespace App\Repository;

use App\Entity\AiStudentInformation;
use App\Entity\AiTaught;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiTaught>
 *
 * @method AiTaught|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiTaught|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiTaught[]    findAll()
 * @method AiTaught[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiTaughtRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiTaught::class);
    }

    public function add(AiTaught $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiTaught $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return AiTaught[] Returns an array of AiTaught objects
     */
    public function getAvalaibleCourse(AiStudentInformation $studentInformation, $month): array
    {
        return $this->createQueryBuilder('ait')
            ->innerJoin('ait.semester', 's', 'WITH', 's = :semester')
            ->setParameter('semester', $studentInformation->getSemester())
            ->innerJoin('ait.faculty', 'f', 'WITH', 'f = :faculty')
            ->setParameter('faculty', $studentInformation->getFaculty())
            ->andWhere('ait.month <= :month')
            ->setParameter('month', $month)
            ->orderBy('ait.month', 'DESC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?IeTaught
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}

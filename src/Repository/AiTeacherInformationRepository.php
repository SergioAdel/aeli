<?php

namespace App\Repository;

use App\Entity\AiTeacherInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiTeacherInformation>
 *
 * @method AiTeacherInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiTeacherInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiTeacherInformation[]    findAll()
 * @method AiTeacherInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiTeacherInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiTeacherInformation::class);
    }

    public function add(AiTeacherInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiTeacherInformation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return IeTeacherInformation[] Returns an array of IeTeacherInformation objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?IeTeacherInformation
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

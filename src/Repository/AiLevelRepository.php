<?php

namespace App\Repository;

use App\Entity\AiLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiLevel>
 *
 * @method AiLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiLevel[]    findAll()
 * @method AiLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiLevelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiLevel::class);
    }

    public function add(AiLevel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiLevel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return AiLevel[] Returns an array of AiLevel objects
     */
    public function getLevelForRegistrationFront()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.name != :name')
            ->setParameter('name', "Licence 3")
            ->orderBy('a.name', 'ASC');
    }

    //    public function findOneBySomeField($value): ?IeLevel
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}

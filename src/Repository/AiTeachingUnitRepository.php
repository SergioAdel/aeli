<?php

namespace App\Repository;

use App\Entity\AiTeachingUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiTeachingUnit>
 *
 * @method AiTeachingUnit|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiTeachingUnit|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiTeachingUnit[]    findAll()
 * @method AiTeachingUnit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiTeachingUnitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiTeachingUnit::class);
    }

    public function add(AiTeachingUnit $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiTeachingUnit $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return IeTeachingUnit[] Returns an array of IeTeachingUnit objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?IeTeachingUnit
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

<?php

namespace App\Repository;

use App\Entity\AiVague;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiVague>
 *
 * @method AiVague|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiVague|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiVague[]    findAll()
 * @method AiVague[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiVagueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiVague::class);
    }

    public function add(AiVague $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiVague $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    /**
     * @return AiVague[] Returns an array of AiVague objects
     */
    public function getStartedVague(): array
    {
        return $this->createQueryBuilder('v')
            ->orderBy('v.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}

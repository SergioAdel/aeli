<?php

namespace App\Repository;

use App\Entity\AiCourseContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiCourseContent>
 *
 * @method AiCourseContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiCourseContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiCourseContent[]    findAll()
 * @method AiCourseContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiCourseContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiCourseContent::class);
    }

    public function add(AiCourseContent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiCourseContent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return AiCourseContent[] Returns an array of AiCourseContent objects
     */
    public function getCourse($course, $courseType): array
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.course', 'c', 'WITH', 'c = :course')
            ->setParameter('course', $course)
            ->andWhere('a.courseType = :courseType')
            ->setParameter('courseType', $courseType)
            ->orderBy('a.updatedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }


    //    public function findOneBySomeField($value): ?IeCourseContent
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}

<?php

namespace App\Repository;

use App\Entity\AiPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AiPage>
 *
 * @method AiPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiPage[]    findAll()
 * @method AiPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiPageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiPage::class);
    }

    public function add(AiPage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiPage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return AiPage[] Returns an array of AiPage objects
     */
    public function findNotDeletedPage(): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.isDeleted = 0')
            ->orderBy('a.menuOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiPage[] Returns an array of AiPage objects
     */
    public function findNotDeletedPageOn(): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.isDeleted = 0')
            ->andWhere('a.isActive = 1')
            ->orderBy('a.menuOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiPage[] Returns an array of AiPage objects
     */
    public function findNotDeletedPageOff(): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.isDeleted = 0')
            ->andWhere('a.isActive = 0')
            ->orderBy('a.menuOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiPage[] Returns an array of AiPage objects
     */
    public function getPages(): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.isDeleted = 0')
            ->andWhere('a.isActive = 1')
            ->orderBy('a.menuOrder', 'ASC')
            ->getQuery()
            ->getResult();
    }

    //    public function findOneBySomeField($value): ?IePage
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}

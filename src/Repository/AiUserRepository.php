<?php

namespace App\Repository;

use App\Entity\AiStudentInformation;
use App\Entity\AiUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<AiUser>
 *
 * @method AiUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method AiUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method AiUser[]    findAll()
 * @method AiUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AiUserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AiUser::class);
    }

    public function add(AiUser $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AiUser $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof AiUser) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->add($user, true);
    }

    /**
     * @return AiUser[] Returns an array of AiUser objects
     */
    public function getUserByRoleType($name): array
    {
        return $this->createQueryBuilder('aiu')
            ->innerJoin('aiu.aiRoles', 'air', 'WITH', 'air.name = :name')
            ->setParameter('name', $name)
            ->andWhere('aiu.isDeleted = 0')
            ->orderBy('aiu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return AiUser[] Returns an array of AiUser objects
     */
    public function getRegistrationRequest(): array
    {
        return $this->createQueryBuilder('aiu')
            ->innerJoin('aiu.aiRoles', 'air', 'WITH', "air.name = 'ROLE_STUDENT'")
            ->innerJoin('aiu.studentInformation', 'si', 'WITH', "si.matriculeNumber IS NULL")
            ->andWhere('aiu.isDeleted = 0')
            ->orderBy('aiu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiUser[] Returns an array of AiUser objects
     */
    public function getUserByRoleTypeNonConfirm($name): array
    {
        return $this->createQueryBuilder('aiu')
            ->innerJoin('aiu.aiRoles', 'air', 'WITH', 'air.name = :name')
            ->setParameter('name', $name)
            ->andWhere('aiu.isDeleted = 0')
            ->andWhere('aiu.confirmAt IS NULL')
            ->orderBy('aiu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiUser[] Returns an array of AiUser objects
     */
    public function getUserByRoleTypeNonVerified($name): array
    {
        return $this->createQueryBuilder('aiu')
            ->innerJoin('aiu.aiRoles', 'air', 'WITH', 'air.name = :name')
            ->setParameter('name', $name)
            ->andWhere('aiu.isDeleted = 0')
            ->andWhere('aiu.isVerified = 0')
            ->orderBy('aiu.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return AiUser[] Returns an array of AiUser objects
     */
    public function getUserByStudentInformation(AiStudentInformation $aiStudentInformation)
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.studentInformation', 'si', 'WITH', 'si.id = :studentInformation')
            ->setParameter('studentInformation', $aiStudentInformation->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }

    //    public function findOneBySomeField($value): ?IeUser
    //    {
    //        return $this->createQueryBuilder('i')
    //            ->andWhere('i.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}

<?php

namespace App\Entity;

use App\Repository\AiFacultyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AiFacultyRepository::class)
 * @ORM\Table(name="ai_faculties")
 */
class AiFaculty
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $acronym;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=AiStudentInformation::class, mappedBy="faculty")
     */
    private $studentInformations;

    /**
     * @ORM\OneToMany(targetEntity=AiTaught::class, mappedBy="faculty")
     */
    private $taughts;

    /**
     * @ORM\OneToMany(targetEntity=AiTimetable::class, mappedBy="faculty")
     */
    private $timetables;

    public function __construct()
    {
        $this->studentInformations = new ArrayCollection();
        $this->taughts = new ArrayCollection();
        $this->timetables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAcronym(): ?string
    {
        return $this->acronym;
    }

    public function setAcronym(string $acronym): self
    {
        $this->acronym = $acronym;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, AiStudentInformation>
     */
    public function getStudentInformations(): Collection
    {
        return $this->studentInformations;
    }

    public function addStudentInformations(AiStudentInformation $student): self
    {
        if (!$this->studentInformations->contains($student)) {
            $this->studentInformations[] = $student;
            $student->setFaculty($this);
        }

        return $this;
    }

    public function removeStudentInformations(AiStudentInformation $student): self
    {
        if ($this->studentInformations->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getFaculty() === $this) {
                $student->setFaculty(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection<int, AiTaught>
     */
    public function getTaughts(): Collection
    {
        return $this->taughts;
    }

    public function addTaught(AiTaught $taught): self
    {
        if (!$this->taughts->contains($taught)) {
            $this->taughts[] = $taught;
            $taught->setFaculty($this);
        }

        return $this;
    }

    public function removeTaught(AiTaught $taught): self
    {
        if ($this->taughts->removeElement($taught)) {
            // set the owning side to null (unless already changed)
            if ($taught->getFaculty() === $this) {
                $taught->setFaculty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AiTimetable>
     */
    public function getTimetables(): Collection
    {
        return $this->timetables;
    }

    public function addTimetable(AiTimetable $timetable): self
    {
        if (!$this->timetables->contains($timetable)) {
            $this->timetables[] = $timetable;
            $timetable->setFaculty($this);
        }

        return $this;
    }

    public function removeTimetable(AiTimetable $timetable): self
    {
        if ($this->timetables->removeElement($timetable)) {
            // set the owning side to null (unless already changed)
            if ($timetable->getFaculty() === $this) {
                $timetable->setFaculty(null);
            }
        }

        return $this;
    }
}

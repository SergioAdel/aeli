<?php

namespace App\Entity;

use App\Repository\AiExamSessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AiExamSessionRepository::class)
 * @ORM\Table(name="ai_exam_sessions")
 */
class AiExamSession
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=AiExam::class, mappedBy="session")
     */
    private $exams;

    /**
     * @ORM\OneToMany(targetEntity=AiResults::class, mappedBy="sessionExam")
     */
    private $results;

    public function __construct()
    {
        $this->exams = new ArrayCollection();
        $this->results = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, AiExam>
     */
    public function getExams(): Collection
    {
        return $this->exams;
    }

    public function addExam(AiExam $exam): self
    {
        if (!$this->exams->contains($exam)) {
            $this->exams[] = $exam;
            $exam->setSession($this);
        }

        return $this;
    }

    public function removeExam(AiExam $exam): self
    {
        if ($this->exams->removeElement($exam)) {
            // set the owning side to null (unless already changed)
            if ($exam->getSession() === $this) {
                $exam->setSession(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->type;
    }

    /**
     * @return Collection<int, AiResults>
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(AiResults $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setSessionExam($this);
        }

        return $this;
    }

    public function removeResult(AiResults $result): self
    {
        if ($this->results->removeElement($result)) {
            // set the owning side to null (unless already changed)
            if ($result->getSessionExam() === $this) {
                $result->setSessionExam(null);
            }
        }

        return $this;
    }
}

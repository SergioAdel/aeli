<?php

namespace App\Entity;

use App\Repository\AiLevelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AiLevelRepository::class)
 * @ORM\Table(name="ai_levels")
 */
class AiLevel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=AiDegree::class, inversedBy="levels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $degree;

    /**
     * @ORM\OneToMany(targetEntity=AiStudentInformation::class, mappedBy="level")
     */
    private $studentInformations;

    /**
     * @ORM\OneToMany(targetEntity=AiSemester::class, mappedBy="level")
     */
    private $semesters;

    /**
     * @ORM\OneToMany(targetEntity=AiTimetable::class, mappedBy="level")
     */
    private $timetables;

    public function __construct()
    {
        $this->studentInformations = new ArrayCollection();
        $this->semesters = new ArrayCollection();
        $this->timetables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDegree(): ?AiDegree
    {
        return $this->degree;
    }

    public function setDegree(?AiDegree $degree): self
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * @return Collection<int, AiStudentInformation>
     */
    public function getStudentInformations(): Collection
    {
        return $this->studentInformations;
    }

    public function addStudentInformation(AiStudentInformation $student): self
    {
        if (!$this->studentInformations->contains($student)) {
            $this->studentInformations[] = $student;
            $student->setLevel($this);
        }

        return $this;
    }

    public function removeStudentInformation(AiStudentInformation $student): self
    {
        if ($this->studentInformations->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getLevel() === $this) {
                $student->setLevel(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection<int, AiSemester>
     */
    public function getSemesters(): Collection
    {
        return $this->semesters;
    }

    public function addSemester(AiSemester $semester): self
    {
        if (!$this->semesters->contains($semester)) {
            $this->semesters[] = $semester;
            $semester->setLevel($this);
        }

        return $this;
    }

    public function removeSemester(AiSemester $semester): self
    {
        if ($this->semesters->removeElement($semester)) {
            // set the owning side to null (unless already changed)
            if ($semester->getLevel() === $this) {
                $semester->setLevel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AiTimetable>
     */
    public function getTimetables(): Collection
    {
        return $this->timetables;
    }

    public function addTimetable(AiTimetable $timetable): self
    {
        if (!$this->timetables->contains($timetable)) {
            $this->timetables[] = $timetable;
            $timetable->setLevel($this);
        }

        return $this;
    }

    public function removeTimetable(AiTimetable $timetable): self
    {
        if ($this->timetables->removeElement($timetable)) {
            // set the owning side to null (unless already changed)
            if ($timetable->getLevel() === $this) {
                $timetable->setLevel(null);
            }
        }

        return $this;
    }
}

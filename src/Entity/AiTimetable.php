<?php

namespace App\Entity;

use App\Repository\AiTimetableRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AiTimetableRepository::class)
 *  @ORM\Table(name="ai_timetables")
 */
class AiTimetable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $target;

    /**
     * @ORM\ManyToOne(targetEntity=AiFaculty::class, inversedBy="timetables")
     */
    private $faculty;

    /**
     * @ORM\ManyToOne(targetEntity=AiVague::class, inversedBy="timetables")
     */
    private $vague;

    /**
     * @ORM\ManyToOne(targetEntity=AiSemester::class, inversedBy="timetables")
     */
    private $semester;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity=AiLevel::class, inversedBy="timetables")
     */
    private $level;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTarget(): ?string
    {
        return $this->target;
    }

    public function setTarget(?string $target): self
    {
        $this->target = $target;

        return $this;
    }

    public function getFaculty(): ?AiFaculty
    {
        return $this->faculty;
    }

    public function setFaculty(?AiFaculty $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }

    public function getVague(): ?AiVague
    {
        return $this->vague;
    }

    public function setVague(?AiVague $vague): self
    {
        $this->vague = $vague;

        return $this;
    }

    public function getSemester(): ?AiSemester
    {
        return $this->semester;
    }

    public function setSemester(?AiSemester $semester): self
    {
        $this->semester = $semester;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getLevel(): ?AiLevel
    {
        return $this->level;
    }

    public function setLevel(?AiLevel $level): self
    {
        $this->level = $level;

        return $this;
    }
}

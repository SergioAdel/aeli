<?php

namespace App\Entity;

use App\Repository\AiVagueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AiVagueRepository::class)
 * @ORM\Table(name="ai_vagues")
 */
class AiVague
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $vNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $vDescription;

    /**
     * @ORM\OneToMany(targetEntity=AiStudentInformation::class, mappedBy="vague")
     */
    private $studentInformation;


    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $startAt;

    /**
     * @ORM\OneToMany(targetEntity=AiTimetable::class, mappedBy="vague")
     */
    private $timetables;

    public function __construct()
    {
        $this->studentInformation = new ArrayCollection();
        $this->timetables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVNumber(): ?int
    {
        return $this->vNumber;
    }

    public function setVNumber(int $vNumber): self
    {
        $this->vNumber = $vNumber;

        return $this;
    }

    public function getVDescription(): ?string
    {
        return $this->vDescription;
    }

    public function setVDescription(?string $vDescription): self
    {
        $this->vDescription = $vDescription;

        return $this;
    }

    /**
     * @return Collection<int, AiStudentInformation>
     */
    public function getStudentInformation(): Collection
    {
        return $this->studentInformation;
    }

    public function addStudentInformation(AiStudentInformation $studentInformation): self
    {
        if (!$this->studentInformation->contains($studentInformation)) {
            $this->studentInformation[] = $studentInformation;
            $studentInformation->setVague($this);
        }

        return $this;
    }

    public function removeStudentInformation(AiStudentInformation $studentInformation): self
    {
        if ($this->studentInformation->removeElement($studentInformation)) {
            // set the owning side to null (unless already changed)
            if ($studentInformation->getVague() === $this) {
                $studentInformation->setVague(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return 'Vague ' . $this->getVNumber();
    }

    public function getStartAt(): ?\DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(?\DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * @return Collection<int, AiTimetable>
     */
    public function getTimetables(): Collection
    {
        return $this->timetables;
    }

    public function addTimetable(AiTimetable $timetable): self
    {
        if (!$this->timetables->contains($timetable)) {
            $this->timetables[] = $timetable;
            $timetable->setVague($this);
        }

        return $this;
    }

    public function removeTimetable(AiTimetable $timetable): self
    {
        if ($this->timetables->removeElement($timetable)) {
            // set the owning side to null (unless already changed)
            if ($timetable->getVague() === $this) {
                $timetable->setVague(null);
            }
        }

        return $this;
    }
}

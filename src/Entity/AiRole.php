<?php

namespace App\Entity;

use App\Repository\AiRoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AiRoleRepository::class)
 * @ORM\Table(name="ai_roles")
 */
class AiRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=AiUser::class, mappedBy="aiRoles")
     */
    private $aiUsers;



    public function __construct()
    {
        $this->aiUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, AiUser>
     */
    public function getAiUsers(): Collection
    {
        return $this->aiUsers;
    }

    public function addAiUser(AiUser $aiUser): self
    {
        if (!$this->aiUsers->contains($aiUser)) {
            $this->aiUsers[] = $aiUser;
            $aiUser->addAiRole($this);
        }

        return $this;
    }

    public function removeAiUser(AiUser $aiUser): self
    {
        if ($this->aiUsers->removeElement($aiUser)) {
            $aiUser->removeAiRole($this);
        }

        return $this;
    }
}

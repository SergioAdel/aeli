<?php

namespace App\Entity;

use App\Repository\AiTeacherInformationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AiTeacherInformationRepository::class)
 * @ORM\Table(name="ai_teacher_informations")
 */
class AiTeacherInformation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salary;

    /**
     * @ORM\ManyToOne(targetEntity=AiUser::class, inversedBy="teacherInformations")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=AiTaught::class, mappedBy="teacherInformation")
     */
    private $taughts;



    public function __construct()
    {
        $this->taughts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSalary(): ?string
    {
        return $this->salary;
    }

    public function setSalary(?string $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getUser(): ?AiUser
    {
        return $this->user;
    }

    public function setUser(?AiUser $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, AiTaught>
     */
    public function getTaughts(): Collection
    {
        return $this->taughts;
    }

    public function addTaught(AiTaught $taught): self
    {
        if (!$this->taughts->contains($taught)) {
            $this->taughts[] = $taught;
            $taught->setTeacherInformation($this);
        }

        return $this;
    }

    public function removeTaught(AiTaught $taught): self
    {
        if ($this->taughts->removeElement($taught)) {
            // set the owning side to null (unless already changed)
            if ($taught->getTeacherInformation() === $this) {
                $taught->setTeacherInformation(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->user->getFullName();
    }
}

<?php

namespace App\Entity;

use App\Repository\AiTaughtRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AiTaughtRepository::class)
 * @ORM\Table(name="ai_taughs")
 */
class AiTaught
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $month;

    /**
     * @ORM\ManyToOne(targetEntity=AiFaculty::class, inversedBy="taughts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $faculty;

    /**
     * @ORM\ManyToOne(targetEntity=AiSemester::class, inversedBy="taughts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $semester;

    /**
     * @ORM\ManyToOne(targetEntity=AiTeacherInformation::class, inversedBy="taughts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $teacherInformation;

    /**
     * @ORM\ManyToOne(targetEntity=AiCourse::class, inversedBy="taughts")
     */
    private $course;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMonth(): ?int
    {
        return $this->month;
    }

    public function setMonth(int $month): self
    {
        $this->month = $month;

        return $this;
    }

    public function getFaculty(): ?AiFaculty
    {
        return $this->faculty;
    }

    public function setFaculty(?AiFaculty $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }

    public function getSemester(): ?AiSemester
    {
        return $this->semester;
    }

    public function setSemester(?AiSemester $semester): self
    {
        $this->semester = $semester;

        return $this;
    }

    public function getTeacherInformation(): ?AiTeacherInformation
    {
        return $this->teacherInformation;
    }

    public function setTeacherInformation(?AiTeacherInformation $teacherInformation): self
    {
        $this->teacherInformation = $teacherInformation;

        return $this;
    }

    public function getCourse(): ?AiCourse
    {
        return $this->course;
    }

    public function setCourse(?AiCourse $course): self
    {
        $this->course = $course;

        return $this;
    }
}

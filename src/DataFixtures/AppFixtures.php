<?php

namespace App\DataFixtures;

use App\Entity\AiDegree;
use App\Entity\AiFaculty;
use App\Entity\AiLevel;
use App\Entity\AiRole;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        // ROLES

        $roleTypes = [
            "ROLE_STUDENT" => "Étudiant",
            "ROLE_ADMIN" => "Administrateur",
            "ROLE_TEACHER" => "Proffesseur",
            "ROLE_SCHOOL_MANAGER" => "Géstionnaire d'écolage"
        ];

        $roles = [];
        foreach ($roleTypes as $name => $description) {
            $aiRoles = new AiRole();
            $aiRoles->setName($name)->setDescription($description);
            $roles[] = $aiRoles;
            $manager->persist($aiRoles);
        }

        // DIPLOME

        $degreeTypes = [
            "LICENCE" => "Diplome de licence",
            "MASTER" => "diplome de master"
        ];

        $degrees = [];
        foreach ($degreeTypes as $name => $description) {
            $aiDegree = new AiDegree();
            $aiDegree->setName($name)->setDescription($description);
            $degrees[] = $aiDegree;
            $manager->persist($aiDegree);
        }

        // NIVEAU

        $levelTypes = [
            "Licence 1" => "Licence 1",
            "Licence 2" => "Licence 2",
            "Licence 3" => "Licence 3",
            "Master 1" => "Master 1",
            "Master 2" => "Master 2"
        ];

        $levels = [];
        $counterDegree = 0;
        foreach ($levelTypes as $name => $description) {
            $aiLevel = new AiLevel();
            $aiLevel->setName($name)->setDescription($description);

            if ($counterDegree < 3) {
                $aiLevel->setDegree($degrees[0]);
            } else {
                $aiLevel->setDegree($degrees[1]);
            }
            $counterDegree++;
            $levels[] = $aiLevel;
            $manager->persist($aiLevel);
        }


        // FILIÈRE

        $facultyTypes = [
            'BEL' => "Business Entrepreneurship et Leadership ",
            'SEAF' => "Stratégie d’Entreprise et Administration Financière ",
            'MITD' => "Management International et Transformation Digital ",
            'RHC' => "Ressources Humaines et Communication ",
        ];
        $faculties = [];
        foreach ($facultyTypes as $acronym => $name) {
            $aiFaculty = new AiFaculty();
            $aiFaculty->setName($name)->setAcronym($acronym)->setDescription($name . '.' . $name);
            $faculties[] = $aiFaculty;
            $manager->persist($aiFaculty);
        }

        $manager->flush();
    }
}

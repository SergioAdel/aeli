<?php

namespace App\Controller\SchoolManager;

use DateTime;
use App\Entity\AiPayment;
use App\Form\ClassificationFilterPaimentType;
use Symfony\Component\Mime\Email;
use App\Repository\AiUserRepository;
use App\Repository\AiPaymentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/school-manager", priority=10)
 */
class SchoolManagerController extends AbstractController
{
    /**
     * @Route("/", name="app_school_manager_home")
     */
    public function index(): Response
    {
        return $this->render('school_manager/dashboard.html.twig');
    }

    /**
     * @Route("/payment/request/{type}", name="app_school_manager_payment_request")
     */
    public function paymentRequest(?string $type, Request $request, AiPaymentRepository $aiPaymentRepository): Response
    {
        $requests = $aiPaymentRepository->getRequestUnReadByType($type);
        return $this->render('school_manager/payment/request.html.twig', [
            'requests' => $requests,
            'type' => $type
        ]);
    }


    /**
     * @Route("/payment/validate/{id}", name="app_payment_validate")
     */
    public function paymentValidate(
        AiPayment $aiPayment,
        Request $request,
        MailerInterface $mailer,
        EntityManagerInterface $entityManagerInterface,
        AiUserRepository $aiUserRepository
    ): Response {
        $aiPayment
            ->setState('read')
            ->setDecision('taken')
            ->setObservation($request->request->get('observation_' . $aiPayment->getId()))
            ->setValidatedAt(new \DateTime());
        $studentInformation = $aiPayment->getStudent();
        switch ($aiPayment->getReason()) {
            case 'registration-fee':
                $studentInformation->setRegistrationFee(1);
                break;
            case 'monthly-fees':
                $studentInformation->setMonthlyFees($studentInformation->getMonthlyFees() + ($aiPayment->getAmount() / 200000));
                break;
        }
        $student = $aiUserRepository->getUserByStudentInformation($studentInformation);
        $email = (new TemplatedEmail())
            ->from('aeli@gmail.com')
            ->to($student->getEmail())
            ->subject('aeli africa paiement : ' . $this->getReasonPayment($aiPayment->getReason()) . ' par MVOLA')
            ->htmlTemplate("school_manager/payment/email-template.html.twig")
            ->context([
                'reason' =>  $aiPayment->getReason(),
                'id' =>  $aiPayment->getId(),
                'student' => $student
            ]);
        $mailer->send($email);
        $entityManagerInterface->flush();
        $this->addFlash('success', "Une demande a été validé avec succèss, IDENTIFICATION : " . $aiPayment->getId());
        return $this->redirectToRoute('app_school_manager_payment_request', ['type' => $iePayment->getType()]);
    }

    /**
     * @Route("/payment/refuse/{id}", name="app_payment_refuse")
     */
    public function paymentRefuse(
        AiPayment $aiPayment,
        Request $request,
        MailerInterface $mailer,
        EntityManagerInterface $entityManagerInterface,
        AiUserRepository $aiUserRepository
    ): Response {
        $aiPayment
            ->setState('read')
            ->setDecision('not taken')
            ->setObservation($request->request->get('observation_' . $aiPayment->getId()));
        $studentInformation = $aiPayment->getStudent();
        $student = $aiUserRepository->getUserByStudentInformation($studentInformation);
        $entityManagerInterface->flush();
        $this->addFlash('success', "Une demande a été refusé avec succèss, IDENTIFICATION : " . $aiPayment->getId());
        return $this->redirectToRoute('app_school_manager_payment_request', ['type' => $aiPayment->getType()]);
    }


    /**
     * @Route("/payment/classification", name="app_payment_classification")
     */
    public function classification(
        Request $request,
        AiPaymentRepository $aiPaymentRepository
    ): Response {
        $aiPayment = new AiPayment();
        $form = $this->createForm(ClassificationFilterPaimentType::class, $aiPayment);
        $form->handleRequest($request);
        $payments = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $payments = $aiPaymentRepository->getClassificationFilter($aiPayment);
        }
        return $this->render('school_manager/payment/classification.html.twig', [
            'form' => $form->createView(),
            'payments' => $payments
        ]);
    }



    public function getReasonPayment($reason)
    {
        return [
            'registration-fee' => 'Frais d\'inscription',
            'monthly-fees' => 'Écolage',
        ][$reason];
    }
}

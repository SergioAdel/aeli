<?php

namespace App\Controller;

use App\Entity\AiUser;
use App\Security\EmailVerifier;
use App\Service\StudentService;
use App\Form\RegistrationFormType;
use Symfony\Component\Mime\Address;
use App\Entity\AiStudentInformation;
use App\Form\StudentInformationType;
use App\Repository\AiRoleRepository;
use App\Repository\AiLevelRepository;
use App\Repository\AiFacultyRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\RegistrationStudentFormType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;


class RegistrationController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * @Route("/inscription", name="app_register", priority=9)
     */
    public function register(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface $entityManager,
        AiRoleRepository $aiRoleRepository,
        MailerInterface $mailer,
        StudentService $studentService
    ): Response {

        // if ($this->getUser()) {
        //     return $this->redirectToRoute('app_home');
        // }

        $student = new AiUser();
        $student->setIsDeleted(false);
        $student->setBirthDate(new \DateTime('2000-01-01'));
        $formStudent = $this->createForm(RegistrationStudentFormType::class, $student);
        $formStudent->handleRequest($request);
        if ($formStudent->isSubmitted() && $formStudent->isValid()) {

            $student->setPassword(
                $userPasswordHasher->hashPassword(
                    $student,
                    'aeli.africa-' . $student->getEmail()
                )
            );
            $studentInformation = $studentService->configureDataStudentInformation($request->request->get($formStudent->getName()), $request->files->get($formStudent->getName()));

            $roleStudent = $aiRoleRepository->findOneByName('ROLE_STUDENT');
            if ($roleStudent) {
                $student->addAiRole($roleStudent);
            }
            $student->setStudentInformation($studentInformation);
            $entityManager->persist($studentInformation);
            $entityManager->persist($student);
            $entityManager->flush();


            $email = (new TemplatedEmail())
                ->from('no-reply@gmail.com')
                ->to('admin@aeli.africa')
                ->subject('aeli africa : nouvelle inscription')
                ->htmlTemplate("registration/notification-email.html.twig")
                ->context([
                    'user' =>  $student,
                    'studentInformation' =>  $studentInformation,
                ]);
            $mailer->send($email);


            // On affiche quelque message de confirmation que le compte a été enregistré dans la base de donnée

            return $this->redirectToRoute('app_register_success');
        }
        return $this->render('registration/register.html.twig', [
            'registrationStudentForm' => $formStudent->createView(),
        ]);
    }

    /**
     * @Route("/inscription/success", name="app_register_success")
     */
    public function register_success(): Response
    {
        return $this->render('registration/success.html.twig');
    }

    /**
     * @Route("/verify/email", name="app_verify_email")
     */
    public function verifyUserEmail(Request $request, TranslatorInterface $translator): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $translator->trans($exception->getReason(), [], 'VerifyEmailBundle'));

            return $this->redirectToRoute('app_login');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Your email address has been verified.');

        return $this->redirectToRoute('app_student_home');
    }
}

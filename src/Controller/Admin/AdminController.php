<?php

namespace App\Controller\Admin;

use App\Repository\AiUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", priority=10)
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="app_admin_home")
     */
    public function index(Request $request, AiUserRepository $aiUserRepository): Response
    {
        $users = $aiUserRepository->getRegistrationRequest();
        return $this->render('admin/dashboard.html.twig', [
            'users' => $users,
            'activeMenu' => ' no'
        ]);
    }
}

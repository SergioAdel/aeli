<?php

namespace App\Controller\Admin;

// use ZipArchive;
use App\Entity\AiRole;
use App\Entity\AiUser;
use ZipStream\ZipStream;
use App\Entity\AiFaculty;
use App\Form\UserFormType;
use ZipStream\Option\Archive;
use App\Form\FilterStudentType;
use App\Security\EmailVerifier;
use App\Service\StudentService;
use App\Form\ValidationStudentType;
use Symfony\Component\Mime\Address;
use App\Entity\AiStudentInformation;
use App\Repository\AiRoleRepository;
use App\Repository\AiUserRepository;
use App\Repository\AiLevelRepository;
use App\Repository\AiVagueRepository;
use Symfony\Component\Form\FormError;
use App\Repository\AiFacultyRepository;
use App\Repository\AiSemesterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Repository\AiStudentInformationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/admin", priority=10)
 */
class UserController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    private function getLastRoute(Request $request)
    {
        $referer = $request->headers->get('referer');
        $baseUrl = $request->getBaseUrl();
        return substr($referer, strpos($referer, $baseUrl) + strlen($baseUrl));
    }
    /**
     * @Route("/user", name="app_admin_user")
     */
    public function index(AiUserRepository $aiUserRepository, Request $request): Response
    {
        $users = $aiUserRepository->getUserByRoleType($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/index.html.twig', compact('users'));
    }

    /**
     * @Route("/user/non-confirm", name="app_admin_user_non_confirm")
     */
    public function nonConfirmUser(AiUserRepository $aiUserRepository, Request $request): Response
    {
        $users = $aiUserRepository->getUserByRoleTypeNonConfirm($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/non-confirm.html.twig', compact('users'));
    }

    /**
     * @Route("/user/non-verified", name="app_admin_user_non_verified")
     */
    public function nonVerifiedUser(AiUserRepository $aiUserRepository, Request $request): Response
    {
        $users = $aiUserRepository->getUserByRoleTypeNonVerified($request->query->get('type', 'ROLE_STUDENT'));
        return $this->render('admin/users/non-verified.html.twig', compact('users'));
    }

    /**
     * @Route("/user/{id<\d+>}/confirm", name="app_admin_user_confirm")
     */
    public function confirm(AiUser $aiUser, EntityManagerInterface $entityManagerInterface, Request $request): Response
    {
        $aiUser->setConfirmAt(new \Datetime('now'));
        $entityManagerInterface->flush();
        $this->addFlash('success', 'Confirmation d\'utilisateur : "' . $aiUser->getFullName() . '" avec succès');
        return $this->redirect($this->getLastRoute($request));
    }

    /**
     * @Route("/user/{id<\d+>}/verify", name="app_admin_user_verify")
     */
    public function verify(AiUser $aiUser, EntityManagerInterface $entityManagerInterface, Request $request): Response
    {
        $aiUser->setIsVerified(true);
        $entityManagerInterface->flush();
        $this->addFlash('success', 'Vérification d\'utilisateur : "' . $aiUser->getFullName() . '" avec succès');
        return $this->redirect($this->getLastRoute($request));
    }

    /**
     * @Route("/user/add", name="app_admin_user_add")
     */
    public function add(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager, AiRoleRepository $aiRoleRepository): Response
    {
        $role = $request->query->get('type', 'ROLE_STUDENT');
        $user = new AiUser();
        $user->setIsDeleted(false);
        $user->addAiRole($aiRoleRepository->findOneByName($role));
        $user->setBirthDate(new \DateTime());
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            $this->emailVerifier->sendEmailConfirmation(
                'app_verify_email',
                $user,
                (new TemplatedEmail())
                    ->from(new Address('aeli-africa@gmail.com', 'AELI AFRICA'))
                    ->to($user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
            $this->addFlash('success', 'Création d\'une nouvelle compte avec succèss, une E-mail a été envoyé vers : ' . $user->getEmail() . ' pour la vérification de cette compte.');
            return $this->redirect($this->generateUrl('app_admin_user') . '?type=' . $request->query->get('type', 'ROLE_STUDENT'));
        }

        return $this->render('admin/users/add.html.twig', [
            'form' => $form->createView(),
        ]);

        return $this->render('admin/users/add.html.twig');
    }





    // REMOVE STUDENT

    /**
     * @Route("/user/student/{id<\d+>}/remove", name="app_admin_user_student_remove")
     */
    public function removeStudent(
        AiUser $student,
        EntityManagerInterface $entityManagerInterface,
        AiStudentInformationRepository $aiStudentInformationRepository
    ): Response {
        if ($student) {
            $studentInformation = $aiStudentInformationRepository->getInformationByUser($student);
            if ($studentInformation) {
                $entityManagerInterface->remove($studentInformation);
            }
            $entityManagerInterface->remove($student);
        }
        $entityManagerInterface->flush();
        return $this->render('admin/users/index.html.twig', [
            'users' => []
        ]);
    }





    // MENU INSCRIPTION

    /**
     * @Route("/user/student/download-data/{id<\d+>}", name="app_admin_user_student_download_data")
     */
    public function downloadData(
        AiUser $aiUser,
        Request $request,
        AiVagueRepository $aiVagueRepository,
        AiStudentInformationRepository $aiStudentInformationRepository,
        StudentService $studentService,
        EntityManagerInterface $entityManagerInterface,
        UserPasswordHasherInterface $userPasswordHasher,
        KernelInterface $appKernel
    ): Response {

        $studentInformation = $aiUser->getStudentInformation();

        $zip = new \ZipArchive;


        $tmp_file = 'uploads/studentZipInformations/' . $aiUser->getEmail() . '.zip';

        $filesystem = new Filesystem();
        if ($filesystem->exists($tmp_file)) {
            $filesystem->remove($tmp_file);
        }

        if ($zip->open($tmp_file,  \ZipArchive::CREATE)) {
            if ($studentInformation->getBirthCertificate()) {
                $zip->addFile('uploads/studentInformations/birthCertificates/' . $studentInformation->getBirthCertificate(), 'birthCertificates-' . $studentInformation->getBirthCertificate());
            }
            if ($studentInformation->getCinPhoto()) {
                $zip->addFile('uploads/studentInformations/cinPhotos/' . $studentInformation->getCinPhoto(), 'cinPhotos-' . $studentInformation->getCinPhoto());
            }
            if ($studentInformation->getCurriculumVitae()) {
                $zip->addFile('uploads/studentInformations/curriculumVitaes/' . $studentInformation->getCurriculumVitae(), 'curriculumVitaes-' . $studentInformation->getCurriculumVitae());
            }
            if ($studentInformation->getMotivationLetter()) {
                $zip->addFile('uploads/studentInformations/motivationLetters/' . $studentInformation->getMotivationLetter(), 'motivationLetters-' . $studentInformation->getMotivationLetter());
            }
            if ($studentInformation->getNoteOrCopyOfTheCertifiedDiploma()) {
                $zip->addFile('uploads/studentInformations/noteOrCopyOfTheCertifiedDiplomas/' . $studentInformation->getNoteOrCopyOfTheCertifiedDiploma(), 'noteOrCopyOfTheCertifiedDiplomas-' . $studentInformation->getNoteOrCopyOfTheCertifiedDiploma());
            }
            if ($studentInformation->getResidenceCertificate()) {
                $zip->addFile('uploads/studentInformations/residenceCertificates/' . $studentInformation->getResidenceCertificate(), 'residenceCertificates-' . $studentInformation->getResidenceCertificate());
            }
            $zip->close();

            $filesystem = new Filesystem();
            if ($filesystem->exists($tmp_file)) {
                return $this->file($tmp_file);
            } else {
                $this->addFlash('danger', 'Le dossier de ' . $aiUser->getEmail() . ' est vide ou possède quelque problème!');
            }
        }

        return $this->redirectToRoute('app_admin_user_student_registration_request_detail', ['id' => $aiUser->getId()]);
    }


    /**
     * @Route("/user/student/registration-request", name="app_admin_user_student_registration_request")
     */
    public function registrationRequest(AiUserRepository $aiUserRepository, Request $request): Response
    {
        $users = $aiUserRepository->getRegistrationRequest();
        return $this->render('admin/users/student/registration-request.html.twig', [
            'users' => $users,
            'activeMenu' => ' inscription'
        ]);
    }

    /**
     * @Route("/user/student/registration-request/{id<\d+>}/detail", name="app_admin_user_student_registration_request_detail")
     */
    public function registrationRequestDetail(
        AiUser $aiUser,
        Request $request,
        AiVagueRepository $aiVagueRepository,
        AiStudentInformationRepository $aiStudentInformationRepository,
        StudentService $studentService,
        EntityManagerInterface $entityManagerInterface,
        UserPasswordHasherInterface $userPasswordHasher
    ): Response {


        $studentInformation = $aiUser->getStudentInformation();
        $errorValidation = false;

        $validationStudentForm = $this->createForm(ValidationStudentType::class, $studentInformation, [
            'level' => $studentInformation->getLevel(),
            // 'idNumber' => $studentInformation->getId(),
        ]);

        $validationStudentForm->handleRequest($request);
        if ($validationStudentForm->isSubmitted() && $validationStudentForm->isValid()) {

            $data = $request->request->get($validationStudentForm->getName());
            $matriculeNumber = $studentService->generateMatriculeNumber($studentInformation, sprintf('%03d', (int)$data['numero']));
            $studentInformationExisted = $aiStudentInformationRepository->findByMatriculeNumber($matriculeNumber);
            if ($studentInformationExisted) {
                $this->addFlash('danger', 'Cette numero est déjà prise par un(e) étudiant(e) qui a un numero de matricule : ' . $matriculeNumber);
                $error = new FormError("Valeur déja prise");
                $validationStudentForm->get('numero')->addError($error);
                $errorValidation = true;
            } else {
                $this->addFlash('success', 'Ok : ' . $matriculeNumber);
                $studentInformation->setMatriculeNumber($matriculeNumber);

                // modification de mot de passe
                $aiUser->setPassword(
                    $userPasswordHasher->hashPassword(
                        $aiUser,
                        $aiUser->getPhoneNumber() . '-i.e'
                    )
                );
                $aiUser->setConfirmAt(new \Datetime('now'));
                $entityManagerInterface->flush();
                // On envoie un E-mail ici

                $this->emailVerifier->sendEmailConfirmation(
                    'app_verify_email',
                    $aiUser,
                    (new TemplatedEmail())
                        ->from(new Address('aeli-africa@gmail.com', 'AELI AFRICA'))
                        ->to($aiUser->getEmail())
                        ->subject('Demande d\inscription accepté')
                        ->htmlTemplate('registration/confirmation_email.html.twig')
                );
                $this->addFlash('success', 'Demande d\'inscription accepté : ' . $aiUser->getFullName());
                return $this->redirectToRoute('app_admin_user_student_registration_request');
            }
        }
        return $this->render('admin/users/student/registration-request-detail.html.twig', [
            'user' => $aiUser,
            'errorValidation' => $errorValidation,
            'validationStudentForm' => $validationStudentForm->createView(),
            'activeMenu' => ' inscription'
        ]);
    }







    // MENU ÉTUDIANT
    /**
     * @Route("/user/student/faculty", name="app_admin_user_show")
     */
    public function userStudentShow(AiFacultyRepository $aiFacultyRepository)
    {
        $faculties = $aiFacultyRepository->findAll();
        return $this->render('admin/users/student/faculty.html.twig', [
            'faculties' => $faculties,
            'activeMenu' => ' étudiant'
        ]);
    }

    /**
     * @Route("/user/student/faculty/{id<\d+>}", name="app_admin_user_faculty_show")
     */
    public function userStudentFacultyShow(
        AiFaculty $aiFaculty,
        Request $request,
        AiStudentInformationRepository $aiStudentInformationRepository,
        AiFacultyRepository $aiFacultyRepository,
        AiLevelRepository $aiLevelRepository,
        AiSemesterRepository $aiSemesterRepository,
        AiVagueRepository $aiVagueRepository
    ) {
        $openCard = false;
        $filterStudentForm = $this->createForm(FilterStudentType::class);
        $students = $aiStudentInformationRepository->getRegistratedStudentByFaculty($aiFaculty);
        $filterStudentForm->handleRequest($request);
        if ($filterStudentForm->isSubmitted()) {
            $aiStudentInformation = new AiStudentInformation();
            $data = $request->request->get($filterStudentForm->getName());
            $aiStudentInformation->setType($data['type']);
            $aiStudentInformation->setFaculty($aiFaculty);
            $aiStudentInformation->setLevel($aiLevelRepository->findOneById($data['level']));
            $aiStudentInformation->setVague($aiVagueRepository->findOneById($data['vague']));
            $aiStudentInformation->setSemester($aiSemesterRepository->findOneById($data['semester']));
            $students = $aiStudentInformationRepository->getFilterStudentInformation($aiStudentInformation);
            $openCard = true;
        }
        return $this->render('admin/users/student/faculty-show.html.twig', [
            'faculty' => $aiFaculty,
            'students' => $students,
            'filterStudentForm' => $filterStudentForm->createView(),
            'openCard' => $openCard,
            'activeMenu' => ' étudiant'
        ]);
    }





    // MENU STATUS DE PAIMENT
    /**
     * @Route("/user/student/payment-status", name="app_admin_user_payment_status")
     */
    public function userStudentPaymentStatus(
        AiStudentInformationRepository $aiStudentInformationRepository,
        Request $request,
        AiFacultyRepository $aiFacultyRepository,
        AiLevelRepository $aiLevelRepository,
        AiSemesterRepository $aiSemesterRepository,
        AiVagueRepository $aiVagueRepository
    ) {
        $openCard = false;
        $filterStudentForm = $this->createForm(FilterStudentType::class, null, [
            'faculty' => 'allow'
        ]);
        $aiStudentInformation = new AiStudentInformation();
        $students = $aiStudentInformationRepository->getPaymentStatusStudent($aiStudentInformation);
        $filterStudentForm->handleRequest($request);
        if ($filterStudentForm->isSubmitted()) {
            $aiStudentInformation = new AiStudentInformation();
            $data = $request->request->get($filterStudentForm->getName());
            $aiStudentInformation->setType($data['type']);
            $aiStudentInformation->setFaculty($aiFacultyRepository->findOneById($data['faculty']));
            $aiStudentInformation->setLevel($aiLevelRepository->findOneById($data['level']));
            $aiStudentInformation->setVague($aiVagueRepository->findOneById($data['vague']));
            $aiStudentInformation->setSemester($aiSemesterRepository->findOneById($data['semester']));
            $students = $aiStudentInformationRepository->getPaymentStatusStudent($aiStudentInformation);
            $openCard = true;
        }
        return $this->render('admin/users/student/payment-status.html.twig', [
            'students' => $students,
            'openCard' => $openCard,
            'filterStudentForm' => $filterStudentForm->createView(),
            'activeMenu' => ' status-de-paiment'
        ]);
    }




    // MENU STATUS DES ÉTUDIANTS
    /**
     * @Route("/user/student/status", name="app_admin_user_status")
     */
    public function userStudentStatus(
        AiStudentInformationRepository $aiStudentInformationRepository,
        Request $request,
        AiFacultyRepository $aiFacultyRepository,
        AiLevelRepository $aiLevelRepository,
        AiSemesterRepository $aiSemesterRepository,
        AiVagueRepository $aiVagueRepository
    ) {
        $openCard = false;
        $filterStudentForm = $this->createForm(FilterStudentType::class, null, [
            'faculty' => 'allow'
        ]);
        $aiStudentInformation = new AiStudentInformation();
        $students = $aiStudentInformationRepository->getPaymentStatusStudent($aiStudentInformation);
        $filterStudentForm->handleRequest($request);
        if ($filterStudentForm->isSubmitted()) {
            $aiStudentInformation = new AiStudentInformation();
            $data = $request->request->get($filterStudentForm->getName());
            $aiStudentInformation->setType($data['type']);
            $aiStudentInformation->setFaculty($aiFacultyRepository->findOneById($data['faculty']));
            $aiStudentInformation->setLevel($aiLevelRepository->findOneById($data['level']));
            $aiStudentInformation->setVague($aiVagueRepository->findOneById($data['vague']));
            $aiStudentInformation->setSemester($aiSemesterRepository->findOneById($data['semester']));
            $students = $aiStudentInformationRepository->getPaymentStatusStudent($aiStudentInformation);
            $openCard = true;
        }
        return $this->render('admin/users/student/status.html.twig', [
            'students' => $students,
            'openCard' => $openCard,
            'filterStudentForm' => $filterStudentForm->createView(),
            'activeMenu' => ' status'
        ]);
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\AiVague;
use App\Entity\AiCourse;
use App\Entity\AiTaught;
use App\Form\CourseType;
use App\Form\TaughtType;
use App\Entity\AiTeachingUnit;
use App\Form\StartCourseType;
use App\Form\TeachingUnitType;
use App\Form\VagueType;
use App\Repository\AiVagueRepository;
use Symfony\Component\Form\FormError;
use App\Repository\AiCourseRepository;
use App\Repository\AiTaughtRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\AiTeachingUnitRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/vague", priority=10)
 */
class VagueController extends AbstractController
{
    /**
     * @Route("/list", name="app_admin_vague")
     */
    public function show(Request $request, AiVagueRepository $aiVagueRepository, EntityManagerInterface $entityManagerInterface): Response
    {
        $openModal = false;
        $aiVague = new AiVague();
        $form = $this->createForm(VagueType::class, $aiVague);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($aiVague);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle vague avec succès');
            return $this->redirectToRoute('app_admin_vague');
        }
        $startedVagues = $aiVagueRepository->getStartedVague();
        return $this->render('admin/vagues/list.html.twig', [
            'activeMenu' => ' ',
            'startedVagues' => $startedVagues,
            'form' => $form->createView(),
            'openModal' => $openModal,
        ]);
    }

    /**
     * @Route("/{id<\d+>}/edit", name="app_admin_vague_edit")
     */
    public function edit(Request $request, AiVague $aiVague, EntityManagerInterface $entityManagerInterface): Response
    {
        $form = $this->createForm(VagueType::class, $aiVague);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($aiVague);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Modification avec succès');
            return $this->redirectToRoute('app_admin_vague');
        }
        return $this->render('admin/vagues/edit.html.twig', [
            'activeMenu' => ' ',
            'form' => $form->createView(),
            'vague' => $aiVague,
            'openModal' => false,
        ]);
    }


    /**
     * @Route("/get-start-date/{id<\d+>}/", name="app_admin_vague_get_start_date")
     */
    public function getStartDate(AiVague $aiVague): Response
    {
        return $this->json([
            'vague' => $aiVague->getVNumber(),
            'startedAt' => $aiVague->getStartAt(),
        ]);
    }
}

<?php

namespace App\Controller\Admin;

use App\Repository\AiUserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/message", priority=10)
 */
class MessageController extends AbstractController
{
    /**
     * @Route("/video-chat", name="app_admin_message_video_chat")
     */
    public function videoChat(Request $request, AiUserRepository $aiUserRepository): Response
    {
        $users = $aiUserRepository->getRegistrationRequest();
        return $this->render('admin/messages/video-chat.html.twig', [
            'users' => $users,
            'activeMenu' => ' no'
        ]);
    }
}

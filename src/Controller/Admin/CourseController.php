<?php

namespace App\Controller\Admin;

use App\Entity\AiVague;
use App\Entity\AiCourse;
use App\Entity\AiCourseContent;
use App\Entity\AiTaught;
use App\Form\CourseType;
use App\Form\TaughtType;
use App\Entity\AiTeachingUnit;
use App\Form\CourseContentType;
use App\Form\StartCourseType;
use App\Form\TeachingUnitType;
use App\Form\VagueType;
use App\Repository\AiCourseContentRepository;
use App\Repository\AiVagueRepository;
use Symfony\Component\Form\FormError;
use App\Repository\AiCourseRepository;
use App\Repository\AiTaughtRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\AiTeachingUnitRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/admin/course", priority=10)
 */
class CourseController extends AbstractController
{
    /**
     * @Route("/teaching-unit", name="app_admin_course_teaching_unit")
     */
    public function index(Request $request, AiTeachingUnitRepository $aiTeachingUnitRepository, EntityManagerInterface $entityManagerInterface): Response
    {
        $teachingUnits = $aiTeachingUnitRepository->findAll();

        $teachingUnit = new AiTeachingUnit();
        $form = $this->createForm(TeachingUnitType::class, $teachingUnit);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($teachingUnit);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout avec succèss');
            return $this->redirectToRoute('app_admin_course_teaching_unit');
        }
        return $this->render('admin/courses/teaching-unit.html.twig', [
            'teachingUnits' => $teachingUnits,
            'activeMenu' => ' teaching-unit',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{slug}", name="app_admin_course_show_detail")
     */
    public function showDetail(
        AiCourse $course,
        Request $request,
        EntityManagerInterface $entityManagerInterface,
        AiCourseContentRepository $aiCourseContentRepository
    ): Response {
        $courseContent = new AiCourseContent();
        $form = $this->createForm(CourseContentType::class, $courseContent);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $courseContent->setCourse($course);
            $entityManagerInterface->persist($courseContent);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout du contenu du cours ' . $course->getTitle() . ' [type:' . $courseContent->getCourseType() . ']avec succèss');
            return $this->redirectToRoute('app_admin_course_show_detail', ['slug' => $course->getSlug()]);
        }
        $lessons = $aiCourseContentRepository->getCourse($course, "LESSON");
        $exercices = $aiCourseContentRepository->getCourse($course, "EXERCICE");
        $corrections = $aiCourseContentRepository->getCourse($course, "CORRECTION");
        return $this->render('admin/courses/show-detail.html.twig', [
            'activeMenu' => ' list',
            'form' => $form->createView(),
            'course' => $course,
            'lessons' => $lessons,
            'exercices' => $exercices,
            'corrections' => $corrections,
        ]);
    }



    /**
     * @Route("/download/course/content/file/{id}", name="app_admin_course_download_file")
     */
    public function downloadFile(
        AiCourseContent $aiCourseContent
    ): Response {

        return $this->file('uploads/courses/content/' . $aiCourseContent->getFile());
    }

    /**
     * @Route("/list", name="app_admin_course_list")
     */
    public function list(Request $request, AiCourseRepository $aiCourseRepository, EntityManagerInterface $entityManagerInterface, SluggerInterface $sluggerInterface): Response
    {
        $cours = $aiCourseRepository->findAll();

        $course = new AiCourse();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $course->setSlug($sluggerInterface->slug($course->getTitle()));
            $entityManagerInterface->persist($course);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout matière avec succèss');
            return $this->redirectToRoute('app_admin_course_list');
        }
        return $this->render('admin/courses/list.html.twig', [
            'courses' => $cours,
            'activeMenu' => ' list',
            'form' => $form->createView(),
            'openModal' => false,
        ]);
    }

    /**
     * @Route("/teaching-course", name="app_admin_course_teaching_course")
     */
    public function teachingCourse(Request $request, AiTaughtRepository $aiTaughtRepository, EntityManagerInterface $entityManagerInterface): Response
    {
        $taugths = $aiTaughtRepository->findAll();

        $taugth = new AiTaught();
        $form = $this->createForm(TaughtType::class, $taugth);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($taugth);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout Formation avec succès');
            return $this->redirectToRoute('app_admin_course_teaching_course');
        }
        return $this->render('admin/courses/teaching-course.html.twig', [
            'taugths' => $taugths,
            'openCard' => false,
            'activeMenu' => ' teaching-course',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/starting", name="app_admin_course_starting")
     */
    public function starting(Request $request, AiVagueRepository $aiVagueRepository, EntityManagerInterface $entityManagerInterface): Response
    {

        $openModal = false;
        $aiVague = new AiVague();
        $form = $this->createForm(VagueType::class, $aiVague);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($aiVague);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle vague avec succès');
            return $this->redirectToRoute('app_admin_course_starting');
        }
        $startedVagues = $aiVagueRepository->getStartedVague();
        return $this->render('admin/courses/starting.html.twig', [
            'activeMenu' => ' ',
            'startedVagues' => $startedVagues,
            'form' => $form->createView(),
            'openModal' => $openModal,
        ]);
    }

    /**
     * @Route("/starting/{id<\d+>}", name="app_admin_course_starting_edit")
     */
    public function edit(Request $request, AiVague $aiVague, EntityManagerInterface $entityManagerInterface): Response
    {

        $aiVague = new AiVague();
        $form = $this->createForm(VagueType::class, $aiVague);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($aiVague);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle date d\'entrée avec succès');
            return $this->redirectToRoute('app_admin_course_starting');
        }
        return $this->render('admin/courses/edit-vague.html.twig', [
            'activeMenu' => ' ',
            'startedVagues' => $aiVague,
            'form' => $form->createView(),
            'openModal' => false,
        ]);
    }
}

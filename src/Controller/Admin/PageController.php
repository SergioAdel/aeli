<?php

namespace App\Controller\Admin;

use App\Entity\AiPage;
use App\Form\AiPageType;
use App\Repository\AiPageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

/**
 * @Route("/admin", priority=10)
 */
class PageController extends AbstractController
{
    /**
     * @Route("/page", name="app_admin_page")
     */
    public function index(AiPageRepository $aiPageRepository): Response
    {
        return $this->render('admin/pages/index.html.twig', [
            'pages' => $aiPageRepository->findNotDeletedPage(),
            'activeMenu' => ' page-list'
        ]);
    }
    /**
     * @Route("/page/on", name="app_admin_page_on")
     */
    public function on(AiPageRepository $aiPageRepository): Response
    {
        return $this->render('admin/pages/on.html.twig', [
            'pages' => $aiPageRepository->findNotDeletedPageOn(),
            'activeMenu' => ' page-active'
        ]);
    }
    /**
     * @Route("/page/off", name="app_admin_page_off")
     */
    public function off(AiPageRepository $aiPageRepository): Response
    {
        return $this->render('admin/pages/off.html.twig', [
            'pages' => $aiPageRepository->findNotDeletedPageOff()
        ]);
    }

    /**
     * @Route("/page/render-view/{view}", name="app_admin_page_render_iframe", requirements={"view"=".+"})
     */
    public function render_iframe($view): Response
    {
        return $this->render($view . '.twig');
    }

    /**
     * @Route("/page/{id}/view", name="app_admin_page_view")
     */
    public function viewPage(AiPage $aiPage, Environment $twig): Response
    {
        $noView = false;
        if (!$twig->getLoader()->exists($aiPage->getView() . '.twig')) $noView = true;
        return $this->render('admin/pages/view.html.twig', compact('aiPage', 'noView'));
    }

    /**
     * @Route("/page/add", name="app_admin_page_add")
     */
    public function add(Request $request, EntityManagerInterface $em): Response
    {
        $aiPage = new AiPage();
        $aiPage->setIsDeleted(false);
        $form = $this->createForm(AiPageType::class, $aiPage);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($aiPage);
            $em->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle page ' . $aiPage->getTitle() . ' avec succès');
            return $this->redirectToRoute('app_admin_page');
        }
        return $this->renderForm('admin/pages/add.html.twig', [
            'form' => $form,
            'activeMenu' => ' page-add'
        ]);
    }

    /**
     * @Route("/page/{id}/edit", name="app_admin_page_edit")
     */
    public function edit(AiPage $aiPage, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(AiPageType::class, $aiPage);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Modification de la page ' . $aiPage->getTitle() . ' avec succès');
            return $this->redirectToRoute('app_admin_page');
        }
        // return $this->renderForm('admin/pages/edit.html.twig', compact('aiPage', 'form'));
        return $this->renderForm('admin/pages/edit.html.twig',  [
            'aiPage' => $aiPage,
            'form' => $form,
            'activeMenu' => ' page-edit'
        ]);
    }

    /**
     * @Route("/page/{id}/toggle/active", name="app_admin_page_toggle")
     */
    public function toogle(AiPage $aiPage, EntityManagerInterface $em): Response
    {
        if ($aiPage->isIsActive()) {
            $this->addFlash('danger', 'Désactivation de la page "' . $aiPage->getTitle() . '" avec succèss');
        } else {
            $this->addFlash('success', 'L\'activation de la page "' . $aiPage->getTitle() . '" avec succèss');
        }
        $aiPage->setIsActive(!$aiPage->isIsActive());
        $em->flush();
        return $this->redirectToRoute('app_admin_page');
    }

    /**
     * @Route("/page/{id}/delete", name="app_admin_page_delete")
     */
    public function delete(AiPage $aiPage, EntityManagerInterface $em): Response
    {
        $aiPage->setIsDeleted(true);
        $em->flush();
        $this->addFlash('success', 'Suppression de la page "' . $aiPage->getTitle() . '" avec succèss');
        return $this->redirectToRoute('app_admin_page');
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\AiLevel;
use App\Entity\AiTimetable;
use App\Form\TimetableType;
use App\Form\TimetableFilterType;
use App\Repository\AiFacultyRepository;
use App\Repository\AiLevelRepository;
use App\Repository\AiSemesterRepository;
use App\Repository\AiTimetableRepository;
use App\Repository\AiVagueRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/timetable", priority=10)
 */
class TimetableController extends AbstractController
{
    /**
     * @Route("/", name="app_admin_timetable")
     */
    public function index(AiFacultyRepository $aiFacultyRepository, Request $request): Response
    {
        $randomFaculty = $aiFacultyRepository->findAll()[0];
        $timetable = new AiTimetable();
        $timetable->setFaculty($randomFaculty);
        $form = $this->createForm(TimetableFilterType::class, $timetable);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
        }
        return $this->render('admin/timetables/index.html.twig', [
            'activeMenu' => ' timetable',
            'form' => $form->createView(),
            'faculty' => $timetable->getFaculty(),
            'timetable' => $timetable,
        ]);
    }

    /**
     * @Route("/get-data", name="app_admin_timetable_get_data")
     */
    public function getData(
        AiTimetableRepository $aiTimetableRepository,
        Request $request,
        AiFacultyRepository $aiFacultyRepository,
        AiLevelRepository $aiLevelRepository,
        AiVagueRepository $aiVagueRepository,
        AiSemesterRepository $aiSemesterRepository
    ): Response {
        $target = $request->request->get('target', '');
        $faculty = $aiFacultyRepository->findOneById($request->request->get('faculty', ''));
        $level = $aiFacultyRepository->findOneById($request->request->get('level', ''));
        $vague = $aiFacultyRepository->findOneById($request->request->get('vague', ''));
        $semester = $aiFacultyRepository->findOneById($request->request->get('semester', ''));

        $filter = compact('target', 'faculty', 'level', 'vague', 'semester');

        $timetables = $aiTimetableRepository->filter($filter);
        $data = [];
        foreach ($timetables as $timetable) {
            array_push($data, [
                'title' => $timetable->getTitle(),
                'start' => $timetable->getStartAt(),
                'end' => $timetable->getEndAt(),
                'backgroundColor' => $timetable->getColor(),
                'borderColor' => $timetable->getColor(),
            ]);
        }
        return $this->json($data);
    }


    /**
     * @Route("/add", name="app_admin_timetable_add")
     */
    public function add(Request $request, EntityManagerInterface $entityManagerInterface): Response
    {
        $timetable = new AiTimetable();
        $form = $this->createForm(TimetableType::class, $timetable);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManagerInterface->persist($timetable);
            $entityManagerInterface->flush();
            $this->addFlash('success', 'Ajout d\'une nouvelle emploi du temps avec succès');
            return $this->redirectToRoute('app_admin_timetable');
        }

        return $this->render('admin/timetables/add.html.twig', [
            'activeMenu' => ' timetable',
            'form' => $form->createView(),
        ]);
    }
}

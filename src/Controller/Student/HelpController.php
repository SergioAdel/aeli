<?php

namespace App\Controller\Student;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student", priority=10)
 */
class HelpController extends AbstractController
{

    /**
     * @Route("/help", name="app_student_help")
     */
    public function index(): Response
    {
        $user = $this->getUser();
        return $this->render('student/help/index.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' help',
            'user' => $user
        ]);
    }
}

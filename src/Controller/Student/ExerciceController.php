<?php

namespace App\Controller\Student;

use App\Entity\AiCourse;
use App\Repository\AiVagueRepository;
use App\Repository\AiTaughtRepository;
use App\Repository\AiCourseContentRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student", priority=10)
 */
class ExerciceController extends AbstractController
{

    /**
     * @Route("/exercice", name="app_student_exercice")
     */
    public function index(
        AiVagueRepository $aiVagueRepository,
        AiTaughtRepository $aiTaughtRepository
    ): Response {
        $user = $this->getUser();
        $studentInformation = $this->getUser()->getStudentInformation();
        $studentInformationVauge = $studentInformation->getVague();
        $vagueFound = $aiVagueRepository->findOneByVNumber($studentInformationVauge);
        if ($vagueFound) {
            $dateEnter = $vagueFound->getStartAt();
            $now = new \DateTimeImmutable("now");
            $diff = $dateEnter->diff($now, true);
            $monthDiff = ($diff->m) + 1; // + 1 : pour avoir le différence mois 0
            $formationforStudents = $aiTaughtRepository->getAvalaibleCourse($studentInformation, $monthDiff);
        }
        return $this->render('student/exercice/index.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' exercice',
            'user' => $user,
            'formationforStudents' => $formationforStudents,
        ]);
    }


    /**
     * @Route("/exercice/{slug}", name="app_student_exercice_show")
     */
    public function show(AiCourse $aiCourse, AiCourseContentRepository $aiCourseContentRepository): Response
    {
        $user = $this->getUser();
        $courseContents = $aiCourseContentRepository->getCourse($aiCourse, "EXERCICE");
        return $this->render('student/exercice/show.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' exercice',
            'courseContents' => $courseContents,
            'course' => $aiCourse,
        ]);
    }

    /**
     * @Route("/exercice/{slug}/correction", name="app_student_exercice_show_correction")
     */
    public function correction(AiCourse $aiCourse, AiCourseContentRepository $aiCourseContentRepository): Response
    {
        $user = $this->getUser();
        $courseContents = $aiCourseContentRepository->getCourse($aiCourse, "CORRECTION");
        return $this->render('student/exercice/correction.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' exercice',
            'courseContents' => $courseContents,
            'course' => $aiCourse,
        ]);
    }
}

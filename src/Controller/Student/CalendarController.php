<?php

namespace App\Controller\Student;

use App\Repository\IeTimetableRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student", priority=10)
 */
class CalendarController extends AbstractController
{

    /**
     * @Route("/calendar/show", name="app_student_calendar_show")
     */
    public function show(): Response
    {
        return $this->render('student/calendar/show.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' calendar'
        ]);
    }

    /**
     * @Route("/calendar/get-data", name="app_student_calendar_get_data")
     */
    public function getData(
        IeTimetableRepository $ieTimetableRepository

    ): Response {

        $timetables = $ieTimetableRepository->getTimetables($this->getUser()->getStudentInformation());
        $data = [];
        foreach ($timetables as $timetable) {
            array_push($data, [
                'title' => $timetable->getTitle(),
                'start' => $timetable->getStartAt(),
                'end' => $timetable->getEndAt(),
                'backgroundColor' => $timetable->getColor(),
                'borderColor' => $timetable->getColor(),
            ]);
        }

        // on récupère la date d'entrée
        $enterDate = $this->getUser()->getStudentInformation()->getVague()->getStartAt();
        if ($enterDate) {
            array_push($data, [
                'title' => "Date d'entrée pour la " . $this->getUser()->getStudentInformation()->getVague(),
                'start' => $enterDate,
                'backgroundColor' => "red",
                'borderColor' => "red",
                'allDay' => true,
            ]);
        }

        return $this->json($data);
    }
}

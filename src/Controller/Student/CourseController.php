<?php

namespace App\Controller\Student;

use App\Entity\AiCourse;
use App\Entity\AiTaught;
use App\Entity\AiCourseContent;
use App\Repository\AiVagueRepository;
use App\Repository\AiTaughtRepository;
use App\Repository\AiCourseContentRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/student", priority=10)
 */
class CourseController extends AbstractController
{

    /**
     * @Route("/course", name="app_student_course")
     */
    public function index(
        AiVagueRepository $aiVagueRepository,
        AiTaughtRepository $aiTaughtRepository
    ): Response {
        $user = $this->getUser();
        $studentInformation = $this->getUser()->getStudentInformation();
        $studentInformationVauge = $studentInformation->getVague();
        $vagueFound = $aiVagueRepository->findOneByVNumber($studentInformationVauge);
        if ($vagueFound) {
            $dateEnter = $vagueFound->getStartAt();
            $now = new \DateTimeImmutable("now");
            $diff = $dateEnter->diff($now, true);
            $monthDiff = ($diff->m) + 1; // + 1 : pour avoir le différence mois 0
            $formationforStudents = $ieTaughtRepository->getAvalaibleCourse($studentInformation, $monthDiff);
        }
        return $this->render('student/course/index.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' course',
            'user' => $user,
            'formationforStudents' => $formationforStudents,
        ]);
    }


    /**
     * @Route("/course/{slug}", name="app_student_course_show")
     */
    public function show(AiCourse $aiCourse, AiCourseContentRepository $aiCourseContentRepository): Response
    {
        $user = $this->getUser();
        $courseContents = $aiCourseContentRepository->getCourse($aiCourse, "LESSON");
        return $this->render('student/course/show.html.twig', [
            'disableAllMenu' => false,
            'activeMenu' => ' course',
            'courseContents' => $courseContents,
            'course' => $aiCourse,
        ]);
    }

    /**
     * @Route("/download/course/content/file/{id}", name="app_student_course_download_file")
     */
    public function downloadFile(
        AiCourseContent $aiCourseContent
    ): Response {

        return $this->file('uploads/courses/content/' . $aiCourseContent->getFile());
    }
}
